@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>All Services</h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Services</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="well">
            <p>{{ link_to_route('admin.services.create', 'Add New Service', null, array('class' => 'btn btn-lg btn-success')) }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">



@if ($services->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <td colspan="2">ID</th>
				<th>Name</th>
        <th>Slug</th>
				<th>Categories</th>
        <th>State </th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($services as $service)
				<tr>
          <td colspan="2">{{{ $service->id }}}</td>
          <td>{{{ $service->name }}}</td>
          <td>{{{ $service->slug }}}</td>
					<td>{{{ join(',', $service->categories->pluck('name','id')->all()) }}}</td>
          <td>{!! $service->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>' !!} </td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit'=>'return confirm(\'Delete Post? Are you sure?\');', 'route' => array('admin.services.destroy', $service->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.services.edit', 'Edit', array($service->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
        @foreach (\App\Models\Service::select()->where('parent_id', '=', $service->id)->get() as $child_service)
          <tr>
            <td></td>
            <td>{{{ $child_service->id }}}</td>
            <td>{{{ $child_service->name }}}</td>
            <td>{{{ $child_service->slug }}}</td>
            <td>{{{ join(',', $child_service->categories->pluck('name','id')->all()) }}}</td>
            <td>{!! $child_service->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>' !!} </td>
            <td>
              {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit'=>'return confirm(\'Delete Post? Are you sure?\');', 'route' => array('admin.services.destroy', $child_service->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.services.edit', 'Edit', array($child_service->id), array('class' => 'btn btn-xs btn-info')) }}
            </td>
          </tr>
        @endforeach
			@endforeach
		</tbody>
	</table>
@else
	There are no services
@endif
</div>
</div>
</div>
</div>
</section>

@stop
