@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>
    Create Service
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Services</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
      {{ Form::open(array('route' => 'admin.services.store', 'files'=>true, 'class' => 'form-horizontal')) }}
        <div class="form-group">
          {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
          </div>
        </div>
        <div class="form-group">
          {{ Form::label('parent_id', 'Parent_id:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            {{ Form::select('parent_id', \App\Models\Service::select()->whereNull('parent_id')->pluck('name', 'id')->all(), Input::old('parent_id'), array('class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          {{ Form::label('categories', 'Categories:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            <ul class="list-unstyled" >
              @foreach(\App\Models\Category::all() as $ii=>$category)
                @if($category->isActive())
                <li>
                  {{ Form::checkbox('categories['.$ii.']', $category->id ) }}
                  {{ $category->name }}
                </li>
                @endif
              @endforeach
            </ul>
          </div>
        </div>
        <div class="form-group">
          {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
          </div>
        </div>
        <div class="form-group">
          {{ Form::label('gender', 'Gender:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            {{ Form::select('gender', ['unisex'=>'Unisex', 'male'=>'Male', 'female'=>'Female'], Input::old('gender'), 
                                     array('class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          {{ Form::label('state', 'Active:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            {{ Form::select('active', ['1'=>'Active', '0'=>'Inactive'], Input::old('active'), 
                                     array('class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">&nbsp;</label>
          <div class="col-sm-10">
            {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</section>

@stop
