@extends('layouts.panel-crm')
@section('content')
<section class="content-header">

  <h1>
    Campaigns
    <small>All Slot Campaigns</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">CRM</a></li>
    <li class="active">Campaigns</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">

<div class="row">
  <div class="col-md-12">

    @include('elements.messages')
<div class="well">
    <div class="pull-right btn-group">
      {{ link_to_route('admin.campaigns.create', 'Add New Advert campaign', null, array('class' => 'btn  btn-success')) }}
    </div>
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control mr5 col-md-6', 'placeholder'=>'Search')) }}
    {{ Form::select('merchant_id', ([''=>'Merchant?'] + \App\Models\Merchant::select()->byLocale()->orderby('name', 'asc')->get()->pluck('name', 'id')->all()), Input::get('merchant_id'), array('class'=>'form-control mr5 ', 'style'=>'width:200px;')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>

    @if ($advert_campaigns->count())
    	<table class="table table-striped">
    		<thead>
    			<tr>
            <th>ID</th>
            <th>Merchant</th>
            <th>Starts</th>
            <th>Ends</th>
            <th>Remarks</th>
    				<th>&nbsp;</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach ($advert_campaigns as $advert_campaign)
    				<tr>
              <td>
                {{ link_to_route('admin.campaigns.show', $advert_campaign->id, array($advert_campaign->id) ) }}
              </td>
              <td>{{ isset($merchants[$advert_campaign->merchant_id])?$merchants[$advert_campaign->merchant_id]:'undefined' }}</td>
              <td>{{ $advert_campaign->start_date }}</td>
              <td>{{ $advert_campaign->end_date }}</td>
              <td>{{ nl2br($advert_campaign->description) }}</td>
              <td>
                <div class="btn-group"> 
                  <button class="btn btn-default btn-xs dropdown-toggle" 
                          type="button" data-toggle="dropdown" 
                          aria-haspopup="true" aria-expanded="false"> 
                      <span class="fa fa-cog"></span> 
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right text-center"> 
                    <li>
                      {{ link_to_route('admin.campaigns.edit', 'Edit this Campaign', array($advert_campaign->id) ) }}
                    </li>
                    <li>
                      {{ link_to_route('admin.campaigns.show', 'View Campaign', array($advert_campaign->id) ) }}
                    </li>                    
                    <li>
                      {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm("Are you sure you want to delete this advert_campaign?")', 'method' => 'DELETE', 'route' => array('admin.campaigns.destroy', $advert_campaign->id))) }}
                        <button style="padding:3px 20px;border:0px; background-color: transparent;"> Delete </button>
                      {{ Form::close() }}
                    </li>
                  </ul>
                </div>
              </td>
    				</tr>
            
    			@endforeach
    		</tbody>
    	</table>

      {{ $advert_campaigns->appends($params)->render() }}
    @else
    	There are no advert_campaigns
    @endif
  </div>
</div>
  </div>
</section>


@stop