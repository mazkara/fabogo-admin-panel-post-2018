@extends('layouts.panel-crm')
@section('content')
<section class="content-header">
  <h1>
    Campaign {{'#'.$advert_campaign->id}}
    <small>{{ $advert_campaign->start_date }} to {{ $advert_campaign->end_date }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">CRM</a></li>
    <li class="active">Campaign</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="pull-right">
            <br/>
            <a class="btn btn-success" href="{{ route('admin.adverts.create', array('campaign_id'=>$advert_campaign->id)) }}">Add Ad to Campaign</a>
            {{ link_to_route('admin.campaigns.show', 'Back', array($advert_campaign->id), array('class' => 'btn btn-info')) }}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
  <table class="table table-striped">
    <thead>
      <tr>
        <!--<th>ID</th>-->
        <th>Venue</th>
        <th>Zone</th>
        <th>Service</th>
        <th>Slot</th>
        <th>Remarks</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    @foreach($adverts as $advert)
      <tr>
        <!-- <td>{{ $advert->id }}</td> -->
        <td>{{ isset($venues[$advert->business_id])?$venues[$advert->business_id]:'undefined' }}</td>
        <td>
          {{ isset($zones[$advert->zone_id])?$zones[$advert->zone_id]:'undefined' }}, 
        </td>
        <td>
          {{ isset($services[$advert->service_id])?$services[$advert->service_id]:'undefined' }},
        </td>
        <td>{{ $advert->slot }}</td>
        <td>{{ $advert->description }}</td>
        <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm("Are you sure you want to delete this advert?")', 'method' => 'DELETE', 'route' => array('admin.adverts.single.destroy', $advert->id))) }}
          <!--<input type="hidden" name="ids" value="{{ $advert->ids }}" />-->
          <button class="btn btn-danger" style="border:0px;"> Delete </button>
        {{ Form::close() }}

        </td>
      </tr>
    @endforeach
    </tbody>

  </table>
</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
$(function(){
  $('.item-inline-editable').editable({placement:'bottom'});
});
</script>
@stop