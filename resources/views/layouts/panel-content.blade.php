<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FabogoAdmin | 6.0.0</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/app.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  @yield('head')
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="/js/app.js"></script>

</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-purple f-ixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>FA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Fabogo</b>Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>



        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Tasks Menu -->
            <li class="dropdown tasks-menu">
              <!-- Menu Toggle Button -->
              <a href="javascript::void(0)" class="dropdown-toggle" data-toggle="dropdown">
                {{ mzk_get_current_locale_attrib('name') }}
              </a>
              <ul class="dropdown-menu">
                <?php $cities = mzk_get_cities_array();?>
                @foreach($cities as $city)
                <li><a href="{{ route('change.locale', $city->id) }}">{{ $city->name }}</a></li>
                @endforeach
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="https://www.gravatar.com/avatar/{{md5(mzk_get_signed_in_user_attrib('email'))}}" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{{ mzk_get_signed_in_user_attrib('name') }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="https://www.gravatar.com/avatar/{{md5(mzk_get_signed_in_user_attrib('email'))}}" class="img-circle" alt="User Image">

                  <p>
                    {{ mzk_get_signed_in_user_attrib('name') }}
                    <small>Member since {{ mzk_get_signed_in_user_attrib('created_at') }}</small>
                  </p>
                </li>
                <!-- Menu Body 
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div> -->
                  <div class="pull-right">
                    <a href="{{ route('users.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="https://www.gravatar.com/avatar/{{md5(mzk_get_signed_in_user_attrib('email'))}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>
            {{ mzk_get_signed_in_user_attrib('name') }}
          </p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
       /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="{{ route('admin.businesses.index') }}">
            <i class="fa fa-building"></i> <span>Businesses</span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.groups.index') }}">
            <i class="fa fa-table"></i> <span>Groups</span>
          </a>
        </li>
        <!--
        <li>
          <a href="{{ route('admin.categories.index') }}">
            <i class="fa fa-folder"></i> <span>Categories</span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.services.index') }}">
            <i class="fa fa-play"></i> <span>Services</span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.highlights.index') }}">
            <i class="fa fa-star"></i> <span>Highlights</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)" onclick="return alert('Work in Progress!');" title="{{ route('admin.zones.index') }}">
            <i class="fa fa-map"></i> <span>Zones</span>
          </a>
        </li>-->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 6.0
      </div>
      <strong>
        Copyright &copy; <?php echo date('Y');?> 
        Made with <i class="fa fa-heart text-danger"></i>
        by Team Mazkara. All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>

</div>
<!-- ./wrapper -->


</body>
</html>
