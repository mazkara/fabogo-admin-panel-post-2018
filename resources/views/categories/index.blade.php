@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>All Categories</h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Categories</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="well">
            <p>{{ link_to_route('admin.categories.create', 'Add New Category', null, array('class' => 'btn btn-lg btn-success')) }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          @if ($categories->count())
          	<table class="table table-striped">
          		<thead>
          			<tr>
                  <th>ID</th>
          				<th>Name</th>
          				<th>Description</th>
          				<th> </th>
          				<th>&nbsp;</th>
          			</tr>
          		</thead>
          		<tbody>
          			@foreach ($categories as $category)
          				<tr>
                    <td>{{{ $category->id }}}</td>
          					<td>{{{ $category->name }}}</td>
          					<td>{{{ $category->description }}}</td>
          					<td>
                      {!! $category->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>'!!} 
                    </td>
                    <td>
                      {{ link_to_route('admin.categories.edit', 'Edit', array($category->id), array('class' => 'btn btn-xs btn-info')) }}
                      {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure? Chaos can erupt!')",  'method' => 'DELETE', 'route' => array('admin.categories.destroy', $category->id))) }}
                          {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
                      {{ Form::close() }}
                    </td>
          				</tr>
          			@endforeach
          		</tbody>
          	</table>
          @else
          	There are no categories
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
@stop
