
@extends('layouts.panel-sms')
@section('content')
<section class="content-header">
  <h1>
    Send Sms
    <small>Test Sms engine</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/sms">Sms</a></li>
    <li class="active">Send</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">



<div class="row">
  <div class="col-md-12">
    <br/>
    {!! BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.sms.post'))->encodingType('multipart/form-data') !!}

    @if (Session::get('notice'))
      <div class="alert alert-success">
        {{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}
      </div>
    @endif
    {!! BootForm::text('Phone Number', 'phone')->placeholder('Phone number to sms') !!}
    {!! BootForm::textarea('Body', 'body')->placeholder('Body') !!}

    {!! BootForm::token() !!}
  <div class="form-group">
      <label class="col-sm-4 control-label">&nbsp;</label>
      <div class="col-sm-8">
        {{ Form::submit('Send Sms', array('class' => 'btn  btn-primary')) }}
      </div>
  </div>
  {!! BootForm::close() !!}


  </div>
</div>
  </div>

</section>
@stop
