@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>
    Create Business
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-10 col-md-offset-2">
          @include('businesses.form')
        </div>
      </div>
    </div>
  </div>
</section>
@stop