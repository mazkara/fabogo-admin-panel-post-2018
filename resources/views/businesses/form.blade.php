@if(isset($business))
  {{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'PATCH', 'files'=>true, 'route' => array('admin.businesses.update', $business->id))) }}
@else
  {{ Form::open(array('route' => 'admin.businesses.store','files'=>true, 'class' => 'form-horizontal')) }}
@endif
  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control required', 'required'=>'required', 'placeholder'=>'Name')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('about', 'About:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('about', Input::old('about'), array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('share_link', 'Share link:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('share_link', Input::old('share_link'), array('class'=>'form-control', 'placeholder'=>'Share link')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('chain_id', 'Chain Of:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('chain_id', \App\Models\Group::byType('chain')->byLocale()->pluck('name', 'id')->all(), Input::old('chain_id'), array('class'=>'form-control', 'placeholder'=>'Select Chain?')) }}
    </div>
  </div>


  <div class="form-group">
    {{ Form::label('categories', 'Categories:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      <ul class="list-unstyled" >
        @foreach($categories as $category)
          <li>
            {{ Form::checkbox('categories[]', $category->id, in_array($category->id, $selected_categories) ) }}
            {{ $category->name }}
          </li>
        @endforeach
      </ul>
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('gender_spec', 'Gender spec:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('gender_spec', ['male'=>'male', 'female'=>'female', 'unisex'=>'unisex', 'n/a'=>'n/a'], Input::old('gender_spec'), array('class'=>'form-control required')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('is_home_service', 'Is Home Service:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('is_home_service', ['0'=>'Not Home Service', '1'=>'Yes! Home Service'], Input::old('is_home_service'), array('class'=>'form-control ')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('type', 'Business Type:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('type', \App\Models\Business::getTypes(), Input::old('type'), array('id'=>'business_types_selector', 'required'=>'required', 'class'=>'form-control required', 'placeholder'=>'Business Type?')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('cost_estimate', 'Cost Estimate:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('cost_estimate', \App\Models\Business::getCostOptions(), Input::old('cost_estimate'), array('class'=>'form-control', 'placeholder'=>'Cost Estimate?')) }}
    </div>
  </div>

  <div id="business-zones-holder" class="form-group "  >
      {{ Form::label('zone_id', 'Sub Zone:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        <select id="zone_id" name="zone_id" required="required" class="required form-control" >
          <option value="" selected disabled>Select a Sub zone</option>

          @foreach ($zones as $zone)
            <optgroup label="{{ $zone['name'] }}">{{ $zone['name'] }}
            @foreach ($zone['children'] as $child)
              <option value="{{ $child['id'] }}" 
                      {{ ($child['id'] == (isset($business) ? $business->zone_id : Input::old('zone_id'))?' selected="selected" ':'' ) }}
                >{{ $child['name'] }}</option>
            @endforeach
            </optgroup>
          @endforeach

        </select>

      </div>
  </div>

  <div class="form-group">
    {{ Form::label('highlights', 'Highlights:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">

      <ul class="list-unstyled" >
        @foreach($highlights as $highlight)
          <li>
            {{ Form::checkbox('highlights[]', $highlight->id, in_array($highlight->id, $selected_highlights) ) }}
            {{ $highlight->name }}
          </li>
        @endforeach
      </ul>


    </div>
  </div>
  @if(1==2)
    <div class="form-group">
      {{ Form::label('services', 'Services:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        <select id="services" name="services[]" multiple="multiple" class="form-control" >
          @foreach ($services as $service)
            <optgroup label="{{ $service['name'] }}">{{ $service['name'] }}
              @foreach ($service['children'] as $child)
                <option value="{{ $child['id'] }}" 
                {{ in_array($highlight['id'], $selected_services)?'selected':'' }}
                  >{{ $child['name'] }}</option>
              @endforeach
            </optgroup>
          @endforeach
        </select>
      </div>
    </div>
  @endif



  <div class="form-group">
    {{ Form::label('phone', 'Phone:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10 input_fields_wrap">
      <div class="input-group">
      {{ Form::text('phone_number[]', '', array('class'=>'form-control', 'placeholder'=>'Phone')) }}
      <span class="input-group-btn">
        <a href="javascript:void(0)" class="btn btn-default add-btn" rel="phone"><i class="fa fa-plus"></i></a>
      </span>
    </div>
      @if(isset($business))
        @foreach($business->phone_number as $phone)
          <div class=" pb5 input-group">
          {{ Form::text('phone_number[]', $phone, array('class'=>'form-control', 'placeholder'=>'Phone')) }}
          <span class="input-group-btn">

          <a href="#" class="remove_field  btn btn-danger"><i class="fa fa-times"></i></a>
        </span>
        </div>
        @endforeach
      @endif
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10 input_fields_wrap">
      <div class="input-group pb5">
      {{ Form::text('email[]', '', array('class'=>'form-control', 'placeholder'=>'Email')) }}
      <span class="input-group-btn">
        <a href="javascript:void(0)" class="btn btn-default add-btn" rel="email"><i class="fa fa-plus"></i></a>
      </span>
    </div>
      @if(isset($business))
        @foreach($business->email as $email)
          <div class=" pb5 input-group">
          {{ Form::text('email[]', $email, array('class'=>'form-control', 'placeholder'=>'Email')) }}
          <span class="input-group-btn">

          <a href="#" class="remove_field  btn btn-danger"><i class="fa fa-times"></i></a>
        </span>
        </div>
        @endforeach
      @endif
    </div>
  </div>
  <hr/>
  <div class="form-group">
    {{ Form::label('preferred_phone', 'Preferred Phone:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('preferred_phone', Input::old('preferred_phone'), array('class'=>'form-control', 'placeholder'=>'Preferred Phone')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('preferred_email', 'Preferred Email:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('preferred_email', Input::old('preferred_email'), array('class'=>'form-control', 'placeholder'=>'Preferred Email')) }}
    </div>
  </div>

  <div class="form-group">
      {{ Form::label('website', 'Website:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('website', Input::old('website'), array('class'=>'form-control', 'placeholder'=>'Website')) }}
      </div>
  </div>
  <div class="form-group">
    {{ Form::label('facebook', 'Facebook:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('facebook', Input::old('facebook'), array('class'=>'form-control', 'placeholder'=>'Facebook')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('twitter', 'Twitter:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('twitter', Input::old('twitter'), array('class'=>'form-control', 'placeholder'=>'Twitter')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('google', 'Google Plus:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('google', Input::old('google'), array('class'=>'form-control', 'placeholder'=>'Google Plus')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('active', 'Active:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('active', [ 0=>'Inactive', 1=>'Active'], Input::old('active'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('is_featured', 'Is featured:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      <?php $featureds = [0=>'Not featured']; for($i=1;$i<=8;$i++){$featureds[$i] = $i;};?>
      {{ Form::select('is_featured', $featureds, Input::old('is_featured'), array('class'=>'form-control')) }}
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>
  {{ Form::close() }}

</div>
<script type="text/javascript">
$(function(){
  var max_fields      = 10; //maximum input boxes allowed
  var wrapper         = $(".input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add-btn"); //Add button ID
 
  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
      e.preventDefault();
      if(x < max_fields){ //max input box allowed
          x++; //text box increment
          $(this).parents(".input_fields_wrap").first().append('<div class="pb5 input-group"><input type="text" class="form-control" name="'+$(this).attr('rel')+'[]"/><span class="input-group-btn"><a href="#" class="remove_field btn btn-danger"><i class="fa fa-times"></i></a></span></div>'); //add input box
      }
  });
 
  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parents('.input-group').first().remove(); x--;
  })

})
</script>