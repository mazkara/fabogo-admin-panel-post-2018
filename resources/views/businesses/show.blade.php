@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>
    {{ $business->id}} - {{ $business->name }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">
        Basic
      </h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ ($business->name) }}</dd>
            <dt>Description</dt>
            <dd>{{ ($business->description) }}</dd>
            <dt>Slug</dt>
            <dd>{{ ($business->slug) }}</dd>
            <dt>Zone</dt>
            <dd>{{ ($business->zone_cache).','.($business->city_name) }}</dd>
            <dt>Phone</dt>
            <dd>{{ join(',', $business->phone_number) }}</dd>
            <dt>Email</dt>
            <dd>{{ join(',', $business->email) }}</dd>
            @if($business->group)
              <dt>Chain</dt>
              <dd>{{ $business->group->name }}</dd>
            @endif
            <dt>About</dt>
            <dd>{{ $business->about }}</dd>
            <dt>Preferred Email</dt>
            <dd>
            @if(is_null($business->preferred_email))
              @if(is_null($business->thePreferredEmail()))
                <span class="label label-danger">NO PREFERRED EMAIL</span>
              @else
                {{ $business->thePreferredEmail() }}
                <span class="label label-warning">FALLBACK PHONE</span>
              @endif
            @else
              {{ $business->preferred_email }}
            @endif
            </dd>
            <dt>Preferred Phone</dt>
            <dd>
            @if(is_null($business->preferred_phone))
              @if(is_null($business->thePreferredPhone()))
                <span class="label label-danger">NO PREFERRED PHONE</span>
              @else
                {{ $business->thePreferredPhone() }}
                <span class="label label-warning">FALLBACK PHONE</span>
              @endif
            @else
              {{ $business->preferred_phone }}
            @endif
            </dd>
            <dt>Categories</dt>
            <dd>{{ join(',', $business->categories()->pluck('name', 'name')->all()) }}</dd>
            <dt>Highlights</dt>
            <dd>{{ join(',', $business->highlights()->pluck('name', 'name')->all()) }}</dd>
            <dt>Website</dt>
            <dd>{{ $business->website }}</dd>
            <dt>Facebook</dt>
            <dd>{{ $business->facebook }}</dd>
            <dt>Twitter</dt>
            <dd>{{ $business->twitter }}</dd>
            <dt>Instagram</dt>
            <dd>{{ $business->instagram }}</dd>
            <dt>Google</dt>
            <dd>{{ $business->google }}</dd>
            <dt>CostEstimate</dt>
            <dd>{{ $business->cost_estimate }}</dd>
            <dt>Type</dt>
            <dd>{{ $business->type }}</dd>
            <dt>Is Featured?</dt>
            <dd>{!! $business->isFeatured()?'<span class="label label-success">'.$business->is_featured.'</span>':'<span class="label label-danger">NON FEATURED</span>' !!}</dd>
            <dt> Has F5 Entry?</dt>
            <dd>
              @if($business->hasOldId())
                Venue has entry on F5 that corresponds to ID {{ $business->old_id }}
              @else
                <a href="{{ route('admin.businesses.generate-old-id', $business) }}">Nope - Click to Generate F5 Entry</a>
              @endif
            </dd>
          </dl>
        </div>
      </div>
    </div>
    <div class="box-footer">
      {{ link_to_action('BusinessesController@edit', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
      {{ link_to_action('BusinessesController@resluggify', 'Resluggify', array($business->id), array('class' => 'btn btn-default')) }}
      <!-- {{ link_to_action('BusinessesController@toggleFeatured', 'Toggle Featured', array($business->id), array('class' => 'btn btn-default')) }} -->
      <a href="{{ $business->getLiveUrl() }}" target="_blank" class="btn btn-success">View On Front</a>
    </div>
  </div>
  <div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Photos</h3>
  </div>
  <div class="box-body">
    <div class="row">
      @foreach($business->photos()->orderby('slot', 'asc')->get() as $photo)
        <div class="image-content col-md-2">
          <img src="{{ $photo->image_thumbnail_path }}" class="img-thumbnail" />
        </div>
      @endforeach
    </div>
  </div>
  <div class="box-footer">
    {{ link_to_action('BusinessesController@getPhotos', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
  </div>  
</div>
  <div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Rate Cards</h3>
  </div>
  <div class="box-body">
    <div class="row">
      @foreach($business->rateCards()->orderby('slot', 'asc')->get() as $photo)
        <div class="image-content col-md-2">
          <img src="{{ $photo->image_thumbnail_path }}" class="img-thumbnail" />
        </div>
      @endforeach
    </div>
  </div>
  <div class="box-footer">
    {{ link_to_action('BusinessesController@getRateCards', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
  </div>  
</div>
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Services</h3>
  </div>
  <div class="box-body">
    @if(count($business->services)==0)
    <div class="alert alert-warning">No services</div>
    @else
        <table class="table table-striped">

    @foreach(\App\Models\Service::whereNull('parent_id')->orderby('name', 'asc')->get() as $parent_service)
      @if($business->services()->where('parent_id','=', $parent_service->id)->count() > 0)
        <thead><tr><th colspan="9"><h3>{{ $parent_service->name }}</h3></th></tr>
        <tr>
          <th><small>Name </small> </th>
          <th><small>Description </small> </th>
          <th><small>Price</small> </th>
          <th><small>Lowest Price</small> </th>
          <th><small>Discount(%)</small> </th>
          <th><small>Discounted</small> </th>
          <th><small>Final Price</small> </th>
          <th><small>Duration</small> </th>
          <th></th>
        </tr></thead>
        <tbody>
          @foreach($business->services()->where('parent_id','=', $parent_service->id)->get() as $service)
            <tr>
              <td>{{ $service->pivot->name }}</td>
              <td>{{ $service->pivot->description }}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->price}}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->lowest_price}}</td>
              <td>{{ $service->pivot->discount_percentage}}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->discounted_price}}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->final_price}}</td>
              <td>{{ $service->pivot->duration}}</td>
              <td>

                @if($service->pivot->gender == 'unisex')
                  <i class="fa text-info fa-venus-mars" title="Unisex"></i>
                @elseif($service->pivot->gender == 'female')
                  <i class="fa text-danger fa-venus" title="Female"></i>
                @elseif($service->pivot->gender == 'male')
                  <i class="fa text-primary fa-mars" title="Male"></i>
                @endif

                {!! $service->pivot->active == 1 ? '<span class="label label-success">ACTIVE</span>':'<span class="label label-danger">INACTIVE</span>'!!}
                {!! $service->pivot->visibility == 1 ? '<span class="label label-success">VISIBLE</span>':'<span class="label label-danger">INVISIBLE</span>'!!}
                <a class="btn btn-default btn-xs deletable-service" href="javascript::void(0)">Click to delete?</a>{{ Form::open(array('style' => 'display: inline-block;',  'method' => 'POST', 'onclick'=>'return confirm("Are you sure?")', 'class'=>'service-form-deletable', 'route' => array('admin.businesses.services.destroy', $service->pivot->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
                {{ Form::close() }}<a class="btn btn-default btn-xs un-deletable-service" href="javascript::void(0)">Cancel</a>
</td>
            </tr>
          @endforeach
        </tbody>
      @endif
    @endforeach
        </table>

    @endif


  </div>
  <div class="box-footer">
    {{ link_to_action('BusinessesController@getServices', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
    @if($business->hasChain())
      {{ link_to_action('BusinessesController@getCopyServices', 'Copy Group(s)', array($business->id), array('class' => 'btn btn-default')) }}
    @endif

  </div>  
</div>

<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Timings</h3>
  </div>
  <div class="box-body">
    @foreach($business->timings as $timing)
      <div class="row">
        <div class="col-md-6">
          {{$timing->days_of_week}}
        </div>
        <div class="col-md-6">
          {{$timing->open}} to
          {{$timing->close}}
        </div>
      </div>

    @endforeach
  </div>
  <div class="box-footer">
    {{ link_to_action('BusinessesController@getTimings', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Offers</h3>
  </div>
  <div class="box-body">
    @if(count($business->details->offers)==0)
    <div class="alert alert-warning">No offers here</div>
    @endif

    @foreach($business->details->offers as $offer)
      <div class="row">
        <div class="col-md-8">
          <b>{{ $offer->title }}</b>
        </div>
        <div class="col-md-4">
          {{ $offer->tag}}
        </div>
      </div>

    @endforeach
  </div>
  <div class="box-footer">
    {{ link_to_action('BusinessesController@getOffers', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>

<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Location</h3>
  </div>
  <div class="box-body">
    <dl class="dl-horizontal">
      <dt>Address</dt>
      <dd>{{ $business->address }}</dd>
      <dt>Geolocation latitude</dt>
      <dd>{{ $business->latitude }}</dd>
      <dt>Geolocation longitude</dt>
      <dd>{{ $business->longitude }}</dd>
      <dt>Geolocation zone</dt>
      <dd>{{ $business->zone_cache }}</dd>
      <dt>Geolocation city</dt>
      <dd>{{ $business->city_name }}</dd>
    </dl>
  </div>
  <div class="box-footer">
    {{ link_to_action('BusinessesController@getLocation', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
  </div>  
</div>
</section>








</section>

<script type="text/javascript">
$(function(){
  $('.service-form-deletable').hide();
  $('.un-deletable-service').hide();

  $('.deletable-service').click(function(){
    $(this).hide();
    $(this).next('.service-form-deletable').show();
    $(this).next('.service-form-deletable').next('.un-deletable-service').show();
  });  
  $('.un-deletable-service').click(function(){
    $(this).hide();
    $(this).prev('.service-form-deletable').hide();
    $(this).prev('.service-form-deletable').prev('.deletable-service').show();
  })
})
</script>

@stop