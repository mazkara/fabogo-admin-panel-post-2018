@extends('layouts.panel-content')
@section('content')

<section class="content-header">
  <h1>
    {{$business->name}} Services
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
{{ Form::model($business, array('class' => 'form-horizontal', 
                                'method' => 'POST', 'files'=>true, 
                                'action' => array('BusinessesController@postServices', 
                                                  $business->id))) }}

<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-10 ">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>

        @foreach($services as $parent_service)
          <div class="row">
            <div class="col-md-12"><h3>{{ $parent_service->name }}</h3></div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped">
              @foreach(\App\Models\Service::select()->where('parent_id', '=', $parent_service->id)->orderby('name', 'asc')->get() as $service)
                <tr>
                  <td colspan="9">
                    {{ $service->name }}
                  </td>
                </tr>
                <tr>
                  <td>
                    {{ Form::checkbox('services['.$service->id.'][available]', $service->id, isset($selected_services[$service->id])?true:false, ['class'=>'chk-service ', 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::text('services['.$service->id.'][name]', (isset($selected_services[$service->id])?$selected_services[$service->id]['name']:$service->name), ['class'=>'form-control service-'.$service->id, 'placeholder'=>'Name', 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::text('services['.$service->id.'][description]', (isset($selected_services[$service->id])?$selected_services[$service->id]['description']:$service->name), ['class'=>'form-control service-'.$service->id, 'placeholder'=>'Description', 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::text('services['.$service->id.'][price]', (isset($selected_services[$service->id])?$selected_services[$service->id]['price']:0), ['class'=>'form-control service-'.$service->id, 'placeholder'=>'Price', 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::select('services['.$service->id.'][discount_percentage]', range(0, 99), (isset($selected_services[$service->id])?$selected_services[$service->id]['discount_percentage']:0), ['class'=>'form-control service-'.$service->id, 'placeholder'=>'Discount percentage', 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::select('services['.$service->id.'][gender]',array('unisex'=>'Unisex', 'male'=>'Male', 'female'=>'Female'), (isset($selected_services[$service->id])?$selected_services[$service->id]['gender']:$service->gender), ['class'=>'form-control service-'.$service->id, 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::text('services['.$service->id.'][duration]', (isset($selected_services[$service->id])?$selected_services[$service->id]['duration']:0), ['class'=>'form-control service-'.$service->id, 'placeholder'=>'Duration', 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::select('services['.$service->id.'][active]', array('1'=>'Active', '0'=>'Not Active'),  (isset($selected_services[$service->id])?$selected_services[$service->id]['active']:0), ['class'=>'form-control service-'.$service->id, 'data-service'=>$service->id]) }}
                  </td>
                  <td>
                    {{ Form::select('services['.$service->id.'][visibility]',
                                   array('1'=>'Visible', '0'=>'Not Visible'), (isset($selected_services[$service->id])?$selected_services[$service->id]['visibility']:0),
                                   ['class'=>'form-control service-'.$service->id, 'data-service'=>$service->id]
                                   ) }}
                  </td>                  

                </tr>
              @endforeach
              </table>
            </div>
          </div>
        @endforeach
    </div>
    <div class="box-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
      {{ link_to_route('admin.businesses.show', 'Cancel', array($business->id), array('class' => 'btn btn-warning ')) }}
    </div>
  </div>
</section>
{{ Form::close() }}

<script type="text/javascript">
  $(document).on('click', '.delete-existing', function(){
    $(this).parents('.image-content').first().hide();
    $(this).parents('.image-content').first().find('.deletable').prop('checked', true);
  });

  $(function(){
    $('.chk-service').each(function(v){
      s_id = $(this).data('service');
      console.log(s_id, this, $(this).prop('checked'));
      if($(this).prop('checked')==true){
        $('.service-'+s_id).prop('disabled', false);
      }else{
        $('.service-'+s_id).prop('disabled', true);
      }
    })

    $('.chk-service').change(function(v){
      s_id = $(this).data('service');
      if($(this).prop('checked')==true){
        $('.service-'+s_id).prop('disabled', false);
      }else{
        $('.service-'+s_id).prop('disabled', true);
      }
    })


  });

</script>
@stop
