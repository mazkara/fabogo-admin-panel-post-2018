<?php $replace = '';?>
<?php 
if(isset($offer)):
    $replace = microtime();
else:
    $replace = '_REPLACE_';
endif;?>
<div class="row row-content">
    <div class=" col-md-6">
        {{ Form::text("offers[$replace][title]", isset($offer)?$offer->title:Input::old('title'), array('class'=>'form-control ', 'placeholder'=>'Title of Offer')) }}
    </div>
    <div class=" col-md-4">
        {{ Form::text("offers[$replace][tag]", isset($offer)?$offer->tag:Input::old('tag'), array('class'=>'form-control ', 'placeholder'=>'Tag/Price')) }}
    </div>
    <div class=" col-md-2">
    <a href="javascript:void(0)" class="delete btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
    </div>

</div>
