{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('BusinessesController@postTimings', $business->id))) }}
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif

          <div class="form-group">
            <div class="col-sm-12">
                  <fieldset>
                      <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-success" id="lnk-add-timings">Add Timing(s)</a>
                      <hr/>
                      <div id="timings-here"></div>
                      @if(isset($business))
                          @foreach($business->timings as $timing)
                              @include('businesses.partials.timings-single')
                              <?php unset($timing);?>
                          @endforeach
                      @endif
                  </fieldset>

            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="box-footer">
          <div class="form-group">
              <div class="col-sm-12">
                {{ Form::submit('Save', array('class' => 'btn  btn-primary')) }}
        {{ link_to_route('admin.businesses.show', 'Cancel', array($business->id), array('class' => 'btn btn-warning')) }}

              </div>
          </div>



    </div>
  </div>
</section>
{{ Form::close() }}
<div id="timing_holder"  style="display:none" >@include('businesses.partials.timings-single')</div>


<script>
$(function(){

    $(document).on('click', '.toggles .btn', function(){
      inp = $(this).find('input');
      hd = $(this).parents('.toggles').first().find('input:hidden');
      ctVal = $(hd).val();
      if(!$(this).hasClass('active')){
        $(hd).val( $(hd).val() + $(inp).val() );
      }else{
        $(hd).val(ctVal.replace('/'+$(inp).val()+'/g', ''));
      }
    });

    $('#lnk-add-timings').click(function(){
      html = $('#timing_holder').html();
      nm = new Date().getTime();
      html = html.replace(/_REPLACE_/g, nm);
      console.log(html);
      $('#timings-here').prepend(html);
    });

    $(document).on('click', '.delete-existing', function(){
      $(this).parents('.row-content').first().hide();
      $(this).parents('.row-content').first().find('.deletable').prop('checked', true);
    });

    $(document).on('click', '#timings-here .delete', function(){
      $(this).parents('.row-content').first().remove();
    });

    $(document).on('focus', '.timepicker', function(){
      $(this).timepicker({ 'timeFormat': 'H:i:s' });
    });
});

</script>