@extends('layouts.panel-content')
@section('content')
{{ Form::model($business, array('class' => 'form-horizontal', 
                                'method' => 'POST',
                                'action' => array('BusinessesController@postCopyServices', 
                                                  $business->id))) }}


<section class="content-header">
  <h1>
    {{ $business->id}} - {{ $business->name }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
<section class="content">
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Copy Services</h3>
  </div>
  <div class="box-body">
    <b>The following service data</b>
    @if(count($business->services)==0)
    <div class="alert alert-warning">No services</div>
    @else
        <table class="table table-striped">

    @foreach(\App\Models\Service::whereNull('parent_id')->orderby('name', 'asc')->get() as $parent_service)
      @if($business->services()->where('parent_id','=', $parent_service->id)->count() > 0)
        <thead><tr><th colspan="9"><h3>{{ $parent_service->name }}</h3></th></tr>
        <tr>
          <th><small>Name </small> </th>
          <th><small>Description </small> </th>
          <th><small>Price</small> </th>
          <th><small>Lowest Price</small> </th>
          <th><small>Discount(%)</small> </th>
          <th><small>Discounted</small> </th>
          <th><small>Final Price</small> </th>
          <th><small>Duration</small> </th>
          <th></th>
        </tr></thead>
        <tbody>
          @foreach($business->services()->where('parent_id','=', $parent_service->id)->get() as $service)
            <tr>
              <td>{{ $service->pivot->name }}</td>
              <td>{{ $service->pivot->description }}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->price}}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->lowest_price}}</td>
              <td>{{ $service->pivot->discount_percentage}}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->discounted_price}}</td>
              <td>{{ $business->getCurrency() }} {{ $service->pivot->final_price}}</td>
              <td>{{ $service->pivot->duration}}</td>
              <td>

                @if($service->pivot->gender == 'unisex')
                  <i class="fa text-info fa-venus-mars" title="Unisex"></i>
                @elseif($service->pivot->gender == 'female')
                  <i class="fa text-danger fa-venus" title="Female"></i>
                @elseif($service->pivot->gender == 'male')
                  <i class="fa text-primary fa-mars" title="Male"></i>
                @endif

                {!! $service->pivot->active == 1 ? '<span class="label label-success">ACTIVE</span>':'<span class="label label-danger">INACTIVE</span>'!!}
                {!! $service->pivot->visibility == 1 ? '<span class="label label-success">VISIBLE</span>':'<span class="label label-danger">INVISIBLE</span>'!!}</td>
            </tr>
          @endforeach
        </tbody>
      @endif
    @endforeach
        </table>
    @endif
    <hr/>
    <p><b>...will be copied to the following venues - (check all venues to copy to)</b></p>
    <table class="table">
    @foreach($venues as $venue)
      @if($venue->id != $business->id)
        <tr>
          <td>{!! Form::checkbox('ids[]', $venue->id, '' ) !!}</td>
          <td>{{ $venue->name }}, {{ $venue->zone_cache }}</td>
        </tr>
      @endif
    @endforeach
    </table>
    <div class="alert alert-warning">
      <p>Note: Existing services will be deleted/overridden for the above selected venues with the services from {{ $business->name.' '.$business->zone_cache }} - this cannot be undone.</p>
    </div>

  </div>
  <div class="box-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
      {{ link_to_route('admin.businesses.show', 'Cancel', array($business->id), array('class' => 'btn btn-warning ')) }}
  </div>
</div>

</section>


{{ Form::close() }}


@stop