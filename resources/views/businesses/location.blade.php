@extends('layouts.panel-content')
@section('head')
<script src="https://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>
@stop
@section('content')

<section class="content-header">
  <h1>
    {{$business->name}} Location
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('BusinessesController@postLocation', $business->id))) }}
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="row">
            <div class="col-md-10 ">
                @if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                  </div>
                @endif
            </div>
        </div>
        <?php 
        $default_city = '';
        $default_state = '';
        $landmarkaddress = '';
        if(is_null(Input::old('geolocation_city')) && is_null($business->city_name)){
          $default_city = ($business->zone_cache == '' ? '' : $business->zone_cache);
        }else{
          $default_city = Input::old('geolocation_city');
        }

        if(is_null(Input::old('geolocation_state')) && is_null($business->city_name)){
          $default_state = ($business->city_name);
        }else{
          $default_state = Input::old('geolocation_state');
        }

        if(is_null(Input::old('address'))&& is_null($business->address)){
          $landmarkaddress = ', '.$default_city.', '.$default_state;
        }else{
          $landmarkaddress = Input::old('address');
        }

        ?>
<div class="row">
    <div class="col-md-6">
        <div id="map" style="height:400px;margin-left:20px;">
        </div> 
    </div>
    <div class="col-md-6 text-left" >
        <div class="row">
            <div class="col-md-10">
                <div class="well" >
                    <small>ADDRESS</small>
                    <div id="addressable"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('latitude', 'Latitude:', array('class'=>'col-md-3 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('latitude', Input::old('latitude'), array('class'=>'form-control', 'placeholder'=>'Latitude')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('longitude', 'Longitude:', array('class'=>'col-md-3 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('longitude', Input::old('longitude'), array('class'=>'form-control', 'placeholder'=>'Longitude')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('address', 'Street:', array('class'=>'col-md-3 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('address', Input::old('address'), array('class'=>'form-control', 'placeholder'=>'Address')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('geolocation_city', 'City:', array('class'=>'col-md-3 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('geolocation_city', $default_city, array('class'=>'form-control', 'placeholder'=>'City')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('geolocation_state', 'State:', array('class'=>'col-md-3 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('geolocation_state', $default_state, array('class'=>'form-control', 'placeholder'=>'State')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('geolocation_country', 'Country:', array('class'=>'col-md-3 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('geolocation_country', Input::old('geolocation_country'), array('class'=>'form-control', 'placeholder'=>'Country')) }}
            </div>
        </div>
    </div>
</div>            
      </div>

<div class="box-footer">
      {{ Form::submit('Save', array('class' => 'btn  btn-primary')) }}
      {{ link_to_route('admin.businesses.show', 'Cancel', array($business->id), array('class' => 'btn btn-warning ')) }}

    </div>
</div>

    </div>
  </div>
</section>
{{ Form::close() }}

<script>
$(function(){


    geocoder = new google.maps.Geocoder()
    var setBusinessPosition = function(position){
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        $('#latitude').val(lat).change();
        $('#longitude').val(lng).change();
        lc = new google.maps.LatLng(lat, lng);

        var mapContext = business_map.locationpicker('map');
        mapContext.marker.setPosition(lc);
        mapContext.map.panTo(lc);

        geocoder.geocode({
                    latLng: lc
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        mapContext.locationName = results[0].formatted_address;
                        console.log(results[0]);
                        mapContext.addressComponents = get_address_component_from_google_geocode(results[0].address_components);
                        updateControls(mapContext.addressComponents);
                    }
                });

    };
    var noLocation = function(){

    };

    var updateControls = function(addressComponents, addressDetailed) {
        html = '<dl class="dl-vertical">';
        for(i in addressComponents){
            html+='<dt>'+i+'</dt>';
            html+='<dd>'+addressComponents[i]+'</dd>';
        }
        html+='<dt>Formatted Address</dt>';
        html+='<dd>'+addressDetailed.formattedAddress+'</dd>';

        $('#addressable').html(html);
        $('#geolocation_city').val(addressComponents.city);
        $('#geolocation_state').val(addressComponents.stateOrProvince);
        $('#geolocation_country').val(addressComponents.country);

    };

    var business_map = $('#map').locationpicker({
        radius: 0,
        @if(isset($business))
            location: { latitude:<?php echo $business->latitude == null ? 0 : $business->latitude;?>, longitude:<?php echo $business->longitude == null ? 0 : $business->longitude;?> },
        @endif
        enableAutocomplete: false,
        inputBinding: {
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            updateControls(addressComponents, $(this).locationpicker('map').location);
            $('#geolocated').val(1);            
        },
    });
    $('#link-get-users-location').click(function(){
        $.geolocation.get({win: setBusinessPosition, fail: noLocation});
    })

})

</script>      {{ link_to_route('admin.businesses.show', 'Return', array($business->id), array('class' => 'btn btn-info btn-xs')) }}

@stop
