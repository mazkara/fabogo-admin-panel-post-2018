@extends('layouts.panel-content')
@section('content')
<style type="text/css">
.btn-default.active, .btn-default:hover
{
color: #fff;
background-color: #449d44;
border-color: #398439;
}
</style>
<section class="content-header">
  <h1>
    {{$business->name}} Timings
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>

        @include('businesses.partials.timings')

@stop
