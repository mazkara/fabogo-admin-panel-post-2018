@extends('layouts.panel-content')
@section('content')
<style type="text/css">
.btn-default.active, .btn-default:hover
{
color: #fff;
background-color: #449d44;
border-color: #398439;
}
</style>
<section class="content-header">
  <h1>
    {{$business->name}} Offers
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('BusinessesController@postOffers', $business->id))) }}
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif

          <div class="form-group">
            <div class="col-sm-12">
              <fieldset>
                  <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-success" id="lnk-add-offer">Add Offer(s)</a>
                  <hr/>
                  <div id="offers-here"></div>
                  @if(isset($business))
                      @foreach($business->details->offers as $offer)
                          @include('businesses.partials.offer')
                          <?php unset($offer);?>
                      @endforeach
                  @endif
              </fieldset>

            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="box-footer">
          <div class="form-group">
              <div class="col-sm-12">
                {{ Form::submit('Save', array('class' => 'btn  btn-primary')) }}
        {{ link_to_route('admin.businesses.show', 'Cancel', array($business->id), array('class' => 'btn btn-warning')) }}

              </div>
          </div>



    </div>
  </div>
</section>
{{ Form::close() }}
<div id="offer_holder"  style="display:none" >@include('businesses.partials.offer')</div>


<script>
$(function(){

    $(document).on('click', '.toggles .btn', function(){
      inp = $(this).find('input');
      hd = $(this).parents('.toggles').first().find('input:hidden');
      ctVal = $(hd).val();
      if(!$(this).hasClass('active')){
        $(hd).val( $(hd).val() + $(inp).val() );
      }else{
        $(hd).val(ctVal.replace('/'+$(inp).val()+'/g', ''));
      }
    });

    $('#lnk-add-offer').click(function(){
      html = $('#offer_holder').html();
      nm = new Date().getTime();
      html = html.replace(/_REPLACE_/g, nm);
      console.log(html);
      $('#offers-here').prepend(html);
    });


    $(document).on('click', '.content .delete', function(){
      $(this).parents('.row-content').first().remove();
    });

});

</script>
@stop
