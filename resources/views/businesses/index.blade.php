@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>
    Businesses
    <small>All venues based in {{ mzk_get_current_locale_attrib('name') }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Businesses</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
        <div class="well">
          {{ link_to_route('admin.businesses.create', 'Add New Business', null, array('class' => 'btn  btn-success')) }}
        </div>


<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}

    {{ Form::text('id', Input::get('id'), array('class'=>'form-control col-md-1', 'style'=>'width:50px;', 'placeholder'=>'ID?')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-1', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}

    {{ Form::select('zone', ([''=>'Zone?'] + \App\Models\Zone::byLocale()->orderby('name', 'ASC')->pluck('name', 'id')->all()), Input::get('zone'), array('class'=>'form-control ')) }}   
    {{ Form::select('is_featured', ([''=>'featured?', '0'=>'Not featured', '1'=>'Featured']), Input::get('is_featured'), array('class'=>'form-control ')) }}   
    {{ Form::select('is_chain', ([''=>'chain?', '0'=>'Not Chain', '1'=>'Is Chain']), Input::get('is_chain'), array('class'=>'form-control ')) }}   

    {{ Form::select('chain', ([''=>'Chain?'] + \App\Models\Group::byLocale()->orderby('name', 'ASC')->pluck('name', 'id')->all()), Input::get('chain'), array('class'=>'form-control ', 'style'=>'width:175px')) }}   

    {{ Form::select('sort', ([ ''=>'Sort by?', 
                              'idAsc'=>'ID Asc', 
                              'idDesc'=>'ID Desc', 
                              'nameAsc'=>'Name Asc', 
                              'nameDesc'=>'Name Desc', 
                              'lastUpdateAsc'=>'Last Update Asc',
                              'lastUpdateDesc'=>'Last Update Desc',
                              ]),
                           Input::get('sort'), array('class'=>'form-control', 'style'=>'width:100px;', 'placeholder'=>'Service')) }}

    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>







        @if ($businesses->count())
          <div class="list-group">
            @foreach ($businesses as $business)
              <a href="{{ route('admin.businesses.show', array($business->id) ) }}" class="list-group-item " >
                <div class="pull-right">
                  <small>Last Updated {{{ \Carbon\Carbon::parse($business->updated_at)->diffForHumans(\Carbon\Carbon::now()) }}}</small><br/>

                  {!! $business->isFeatured()?'<span class="label label-success">'.$business->is_featured.'</span>':'<span class="label label-danger">NON FEATURED</span>' !!}
                </div>
                <h4 class="list-group-item-heading">
                  {{{ $business->id }}} -          
                  {{{ $business->name }}}
                  <small>
                    <span class="label label-{{ $business->getRichnessColor() }}" >
                      {{ '# '.$business->popularity }}
                    </span>
                  </small>
                </h4>
                <p class="list-group-item-text">
                  <b>Phone:</b>{{{ join(',', $business->phone_number) }}}
                  <b>Email:</b>{{{ join(',', $business->email) }}}
                  <br/>
                  <b>Website:</b>{{{ $business->website }}}
                  <b>Zone:</b>{{{ $business->zone_cache }}}
                </p>
                <p class="list-group-item-text">
                  <span class="label label-{{ $business->active == '1' ?'success':'warning'}}">
                    {{{ $business->active == '1'?'active':'inactive' }}}
                  </span>
                  @if(0 ==($business->latitude + $business->longitude))
                    <span class="label label-danger">Location Not Set</span>
                  @endif
                </p>
              </a>
            @endforeach      
          </div>

          <div class="row">
            <div class="col-md-12">
          {{ $businesses->appends($params)->render() }}
          <div class="pull-right">
            {{ count($businesses) }} / {{ $businesses->total() }} entries
          </div></div>
        </div>
        @else
          There are no businesses
        @endif
      </div>
    </div>
  </div>
</div>
</section>
@stop
