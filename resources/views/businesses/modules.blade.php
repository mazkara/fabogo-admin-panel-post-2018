{{ Form::model($business, array('class' => 'form-horizontal', 
                                'method' => 'POST', 'files'=>true, 
                                'action' => array('BusinessesController@postModules', 
                                                  $business->id))) }}
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-10 ">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
      <table class="table">
        <thead>
          <tr>
            <th>Module</th>
            <td>Access</td>
          </tr>
        </thead>
        <tbody>
          @foreach($modules as $module)
            <tr>
              <td>{{ $module->name }}</td>
              <td>{!! Form::select('modules['.$module->id.'][active]', array('0'=>'No Access', '3'=>'Full Access'), 
                        (isset($selected_modules[$module->id])?$selected_modules[$module->id]:0),  array('class'=>'form-control') ) !!}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
</section>
{{ Form::close() }}
