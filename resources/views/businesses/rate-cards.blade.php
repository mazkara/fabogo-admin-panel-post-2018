@extends('layouts.panel-content')
@section('content')

<section class="content-header">
  <h1>
    {{$business->name}} Rate Cards
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Business</li>
  </ol>
</section>
{{ Form::model($business, array('class' => 'form-horizontal', 
                                'method' => 'POST', 'files'=>true, 
                                'action' => array('BusinessesController@postRateCards', 
                                                  $business->id))) }}

<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-10 ">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        @foreach($business->rateCards()->orderby('slot', 'asc')->get() as $photo)

          <div class="image-content col-md-3">
            <div class="row">
              <div class="col-md-12">
                <img src="{{ $photo->image_thumbnail_path }}" class="img-thumbnail" />
              </div>
            </div>
            <div class="well">
            <div class="row">
              <div class="col-md-9">
              {{ Form::text("orderables[$photo->id]", $photo->slot, array('class'=>'form-control') ) }}
              </div>
              <div class="col-md-2">
                {{ Form::checkbox("deletablePhotos[]", $photo->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
                <a href="javascript:void(0)" class="mr5 delete-existing btn btn-sm btn-danger">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            </div>

          </div>
        @endforeach
      </div>
      <div class="row">
        <div class="col-sm-10 ">
          {{ Form::file('images[]', array( 'accept'=>"image/*", 'id'=>'input-bulk-image-upload', 'multiple'=>'true')) }}
        </div>
      </div>
    </div>
    <div class="box-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
      {{ link_to_route('admin.businesses.show', 'Cancel', array($business->id), array('class' => 'btn btn-warning ')) }}
    </div>
  </div>
</section>
{{ Form::close() }}

<script type="text/javascript">
  $(document).on('click', '.delete-existing', function(){
    $(this).parents('.image-content').first().hide();
    $(this).parents('.image-content').first().find('.deletable').prop('checked', true);
  });

</script>
@stop
