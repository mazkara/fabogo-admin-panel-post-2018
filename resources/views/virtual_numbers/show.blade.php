@extends('layouts.admin-crm')
@section('content')

<h1>Show Virtual_number</h1>

<p>{{ link_to_route('admin.virtual_numbers.index', 'Return to All virtual_numbers', null, array('class'=>'btn btn-lg btn-primary')) }}</p>
@if(!$current_business)
  <div class="alert alert-danger">Not current allocated to any business</div>
@else
  Currently allocated to {{ $current_business->name }}
@endif
<table class="table table-striped">
	<thead>
		<tr>
			<th>Body</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $virtual_number->body }}}</td>
			<td>{{{ $virtual_number->state }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.virtual_numbers.destroy', $virtual_number->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.virtual_numbers.edit', 'Edit', array($virtual_number->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop