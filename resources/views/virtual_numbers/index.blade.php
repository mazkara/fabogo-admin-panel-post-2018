@extends('layouts.panel-crm')
@section('content')
<section class="content-header">
  <h1>
    Virtual numbers
    <small>All Virtual numbers</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">CRM</a></li>
    <li class="active">Virtual Numbers</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
  <div class="box-body">

<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control mr5 col-md-6', 'placeholder'=>'Search')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
    <div class="pull-right">
      <p>{{ link_to_route('admin.virtual_numbers.create', 'Add New Virtual number', null, array('class' => 'btn btn-sm btn-success')) }}</p>

  </div>

</div>

@if ($virtual_numbers->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
        <th>Body</th>
        <th>Current?</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($virtual_numbers as $virtual_number)
      <?php $has_current_business = $virtual_number->current_business();?>


				<tr>
          <td>{{{ $virtual_number->id }}}</td>
					<td>{{{ $virtual_number->body }}}</td>
          <td>{!! $has_current_business!=false ? $virtual_number->current_business_name() :'<span class="label label-default">none</span>' !!}</td>
          <td>
            {{ link_to_route('admin.virtual_numbers.edit', 'Edit', array($virtual_number->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $virtual_numbers->render() }}
  <div class="pull-right">
    {{ count($virtual_numbers) }} / {{ $virtual_numbers->total() }} entries
  </div></div>
</div>


@else
	There are no virtual_numbers
@endif
</div></div>

</div>
</div>
</section>
@stop
