@extends('layouts.panel-crm')
@section('content')
<section class="content-header">
  <h1>
    Create Virtual number
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">CRM</a></li>
    <li class="active">Virtual Numbers</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-10 col-md-offset-2">
          @if ($errors->any())
          	<div class="alert alert-danger">
        	    <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
              </ul>
          	</div>
          @endif
        </div>
      </div>
      {{ Form::open(array('route' => 'admin.virtual_numbers.store', 'class' => 'form-horizontal')) }}
        <div class="form-group">
          {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            {{ Form::text('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'Body')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">&nbsp;</label>
          <div class="col-sm-10">
            {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
            {{ link_to_route('admin.virtual_numbers.index', 'Cancel', '', array('class' => 'btn btn-lg btn-default')) }}
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</section>
@stop