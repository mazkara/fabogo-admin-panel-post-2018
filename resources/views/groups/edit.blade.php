@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>
    Edit Group
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Groups</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
{{ Form::model($group, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.groups.update', $group->id))) }}

    <div class="form-group">
        {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
        </div>
    </div>

<div class="form-group">
  {{ Form::label('cover', 'Cover Photo', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::file('cover', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
    @if(isset($group))
      @if($group->image)
        <img src="{{ $group->image->image_thumbnail_url }}" class="img-thumbnail" />
        {{ Form::checkbox("deletablePhotos", $group->image_id, false ) }}
        Delete?
      @endif
    @endif

  </div>
</div>
<div class="form-group">
  {{ Form::label('is_custom_active', 'Activate Custom Page:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::select('is_custom_active',  [0=>'Inactive', '1'=>'Activated'],   Input::old('is_custom_active'),array('class'=>'form-control', 'placeholder'=>'State')) }}
  </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.groups.show', 'Cancel', $group->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</section>
<script type="text/javascript">
$(function(){
  $('#business-search').autocomplete({
    source:'/content/groups/get-outlets',
    select: function(event, ui){
      $('#businesses-holder').append(ui.item.html);
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( '<div >' + item.id + ' - ' +item.name + '<small>('+item.zone_cache+')</small></div>' )
      .appendTo( ul );
  };  
  $('.datepicker').datepicker({ format: "yyyy-mm-dd", autoclose:true });

  $(document).on('click', '.deletable-link', function(){
    $(this).prevAll('.deletable').first().attr('checked', true);
    $(this).parents('li').first().css('backgroundColor', 'red').fadeOut( "slow", function() {
      $(this).remove();
    });
  });


});
</script>

@stop
