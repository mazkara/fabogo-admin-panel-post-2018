@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>All Groups</h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Groups</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="well">
        <p>{{ link_to_route('admin.groups.create', 'Add New Group', null, array('class' => 'btn btn-lg btn-success')) }}</p>
      </div>
      <div class="well">
        {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
          {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:550px;', 'placeholder'=>'Search')) }}
          <button type="submit" class="btn btn-default">Filter</button>
        {{ Form::close() }}
      </div>

      @if ($groups->count())
      	<table class="table table-striped">
      		<thead>
      			<tr>
      				<th>Name</th>
      				<th>Type</th>
              <th></th>
      				<th>&nbsp;</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach ($groups as $group)
      				<tr>
      					<td>{{{ $group->name }}}</td>
      					<td>{{{ $group->slug }}}</td>
                <td>
                  {{$group->businesses()->count() }} Venues(s)
                </td>
                <td>
                  {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.groups.destroy', $group->id))) }}
                      {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                  {{ Form::close() }}
                  {{ link_to_route('admin.groups.edit', 'Edit', array($group->id), array('class' => 'btn btn-info')) }}
                </td>
      				</tr>
      			@endforeach
      		</tbody>
      	</table>
        <div class="row">
          <div class="col-md-12">
            {{ $groups->render() }}
            <div class="pull-right">
              {{ count($groups) }} / {{ $groups->total() }} entries
            </div>
          </div>
        </div>
      @else
      	There are no groups
      @endif
    </div>
  </div>
</section>

@stop
