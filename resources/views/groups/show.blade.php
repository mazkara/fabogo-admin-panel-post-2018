@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>
    {{{ $group->name }}}
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Groups</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
<p>{{ link_to_route('admin.groups.index', 'Return to All groups', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Type</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $group->name }}}</td>
			<td>{{{ $group->type }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.groups.destroy', $group->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.groups.edit', 'Edit', array($group->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>
</div>
</div>
</div>
</div>
</section>
@stop
