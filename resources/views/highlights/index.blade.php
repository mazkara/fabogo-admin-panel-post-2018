@extends('layouts.panel-content')
@section('content')
<section class="content-header">
  <h1>All Highlights</h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/content">Content</a></li>
    <li class="active">Highlights</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="well">
          <p>{{ link_to_route('admin.highlights.create', 'Add New Highlight', null, array('class' => 'btn btn-lg btn-success')) }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">



@if ($highlights->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Description</th>
        <th> </th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($highlights as $highlight)
				<tr>
          <td>{{ $highlight->id }}</td>
					<td>{{ $highlight->name }}</td>
					<td>{{ $highlight->slug }}</td>
					<td>{{ $highlight->description }}</td>
          <td>{!! $highlight->isActive()?'<span class="label label-success">ACTIVE</span>':'<span class="label label-default">INACTIVE</span>' !!} </td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;',  'method' => 'DELETE', 'route' => array('admin.highlights.destroy', $highlight->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.highlights.edit', 'Edit', array($highlight->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no highlights
@endif
</div>
</div>
</div>
</div>
</section>
@stop
