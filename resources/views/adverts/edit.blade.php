@extends('layouts.panel-crm')
@section('content')
<section class="content-header">
  <h1>
    Campaign {{ '#'.$advert_campaign->id }} 
    <small>Edit Advert {{ '#'.$advert->id }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">CRM</a></li>
    <li class="active">Campaign</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
  <div class="box-body">
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>
</div>
</div>
</section>
@include('adverts.form')
@stop

