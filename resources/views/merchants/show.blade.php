@extends('layouts.panel-crm')
@section('content')
<section class="content-header">
  <h1>
    {{ $merchant->name }}
  </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="pull-right">
                {{ link_to_route('admin.merchants.edit', 'Edit', array($merchant->id), array('class' => 'btn btn-info btn-xs')) }}
                  {{ Form::open(array('style' => 'display: inline-block;margin:0px 3px 0px 3px;', 'method' => 'DELETE', 
                                      'onsubmit'=>"return confirm('Deleting this merchant will delete all associated virtual number allocations and its invoices - are you sure you want to delete this merchant? You have been warned...');",
                                      'route' => array('admin.merchants.destroy', $merchant->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
                  {{ Form::close() }}
              </div>
              <h5>
                <i class="fa fa-envelope" ></i>&nbsp;{{ $merchant->email }}&nbsp;&nbsp;
                <i class="fa fa-map-marker"></i>&nbsp;{{ $merchant->city_name() }}&nbsp;&nbsp;
                <b>Sales POC:</b>&nbsp;{{ $merchant->poc_name() }}&nbsp;&nbsp;
                @if(!empty($merchant->tan))
                  <b>TAN:</b>
                  {{ $merchant->tan }}
                @endif
              </h5>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <h5>BUSINESSES ALLOCATED</h5>
              @foreach($merchant->businesses as $business)
                <div class="panel panel-default">
                  <div class="panel-body">
                  <div class="media">

                    <span class="pull-right">
                      <a href="javascript:void(0)" data-business="{{$business->id}}" class="btn btn-sm lnk-edit-module-access btn-default">Module Access</a>
                    </span>


                    <h4 class="media-heading">
                      ({{$business->id}}) {{$business->name}}
                    </h4>
                    <i class="glyphicon glyphicon-map-marker"></i> {{ $business->zone_cache }}<br/>
                   
              @if($business->isCurrentlyAllocatedAVirtualNumber() == true)
                <p title="Allocated Number">
                  <i class="fa fa-phone"></i> {{ $business->current_virtual_number_allocation_number() }}
                  {{ link_to_route('admin.virtual_numbers.for.business.deallocate',
                                   'Deallocate', 
                                   array($business->current_virtual_number_allocation_id()), 
                                   array('class' => 'btn btn-xs btn-warning',
                                          'onclick'=>"return confirm('Are you sure?')"
                                          )) }}

                </p>
              @else
                <p>
                  <a  title="No Number allocated - click to allocate" 
                      href="{{route('admin.virtual_numbers.for.business.allocate', $business->id)}}" 
                      class="text-danger">
                    <i class="fa fa-phone"></i>  No Virtual Number (Click to allocate)
                  </a> 
                </p>
              @endif

                  </div>
                </div>
                <div class="panel-footer">
                  <a title="Accessible by admins and sales pocs only" 
                    class="btn btn-sm btn-warning" target="_blank" 
                    href="http://partners.fabogo.com/client/login?id=<?php echo Auth::user()->id ;
                    ?>&token=<?php echo Auth::user()->access_token; 
                    ?>&b_id=<?php echo $business->id ;?>">Clients View <i class="fa fa-info-circle"></i></a>
                </div>
              </div>
                <hr/>
              @endforeach
            </div>
            <div class="col-md-6">
              <h5>USERS ALLOCATED</h5>
              @foreach($merchant->users as $user)
                <div class="media">
                  <div class="">
                    <h4 class="media-heading">
                      {{$user->name}}
                    </h4> 
                    <small>{{$user->email}}</small>
                  </div>
                </div>
                <hr/>
              @endforeach
            </div>
          </div>
          <p><a href="{{ route('admin.merchants.index') }}" class='btn btn-default'><i class="fa fa-chevron-left"></i> Back</a></p>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modules-form">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
  $(function(){
    $('.lnk-edit-module-access').click(function(){
      $.ajax({
        url: '/content/businesses/'+$(this).data('business')+'/modules',
        method: 'GET',
      }).done(function(result){
        $('#modules-form').modal('show');
        $('.modal-title').html(result.title);        
        $('.modal-body').html(result.body);
      });
    });
  });
</script>
@stop