@extends('layouts.panel-crm')
@section('content')
<section class="content-header">
  <h1>
    Merchants
    <small>All merchants listed for {{ mzk_get_current_locale_attrib('name') }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">CRM</a></li>
    <li class="active">Merchants</li>
  </ol>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body">

      <div class="row">
        <div class="col-md-12">
          <div class="well">
            {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
              {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}
              <button type="submit" class="btn btn-default">Filter</button>
            {{ Form::close() }}
            <div class="pull-right">
              <p>
                {{ link_to_route('admin.merchants.create', 'Add New Merchant', null, array('class' => 'btn btn-sm btn-success')) }}
              </p>
            </div>
            
          </div>
          @if ($merchants->count())
          	<table class="table table-condensed table-striped">
          		<thead>
          			<tr>
                  <th>ID</th>
          				<th>Name</th>
          				<th>Sales POC</th>
                  <th>Registration Date</th>
          				<th>&nbsp;</th>
          			</tr>
          		</thead>

          		<tbody>
          			@foreach ($merchants as $merchant)
          				<tr>
                    <td>{{ $merchant->getDisplayableMerchantID() }}</td>
          					<td>{{ link_to_route('admin.merchants.show', $merchant->name, array($merchant->id)) }}</td>
          					<td>{{{ $merchant->poc_name() }}}</td>
                    <td>{{{ ($merchant->created_at) }}}</td>
                    <td>
                      <a href="{{ route('admin.merchants.show', array($merchant->id) )}}" class="btn btn-xs btn-success">
                        <i class="fa fa-eye"></i>
                      </a>
                    </td>
          				</tr>
          			@endforeach
          		</tbody>
          	</table>
            {{ $merchants->appends($params)->render() }}
          @else
          	There are no merchants
          @endif
</div></div>
    </div>
  </div>
</section>

@stop