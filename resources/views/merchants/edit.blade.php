@extends('layouts.panel-crm')
@section('content')

<section class="content-header">
  <h1>
    Edit Merchant
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/crm">Content</a></li>
    <li class="active">Merchants</li>
  </ol>
</section>

@include('merchants.partials.form')

@stop