<?php

function mzk_get_cities_array($force = true){
  $label = 'mazkara.cities';

  if(\Session::has($label) && ($force != true)){
    return \Session::get($label);
  }else{
    mzk_set_cities_array();
    return \Session::get($label);
  }
}

function mzk_set_cities_array(){
  $label = 'mazkara.cities';
  \Session::put($label, \App\Models\City::select()->get());
}


function mzk_get_default_city(){
  $label = 'mazkara.admin.locale';
  if(\Session::has($label)){
    return \Session::get($label);
  }else{
    $cities = mzk_get_cities_array();
    $city = $cities->first();

    mzk_set_default_city($city);
    return \Session::get($label);
  }
}

function mzk_set_default_city($city){
  $label = 'mazkara.admin.locale';
  \Session::put($label, $city);
}

function mzk_get_localeID(){

  return mzk_get_locale_id();
}

function mzk_get_locale_id(){
  $city = mzk_get_default_city();
  return $city->id;
}

function mzk_get_current_locale_attrib($attrib){
  $city =  mzk_get_default_city();
  if(isset($city->$attrib)){
    return $city->$attrib;
  }else{
    return false;
  }

  
}

function mzk_get_signed_in_user_attrib($attrib){
  $user = \Auth::user();

  if(isset($user->$attrib)){
    return $user->$attrib;
  }else{
    return false;
  }
}


function mzk_route_helper($resource, $prefix = false, $skip = false){
  $a = ['index','create','store','show','edit','update','destroy'];
  if(is_array($skip)){
    foreach($skip as $vv){
      $a = array_diff($a, [$vv]);
    }
  }
  $r = [];
  foreach($a as $v){
    $r[$v] = ($prefix ? $prefix.'.' : '').$resource.'.'.$v;
  }

  return $r;
}

function mzk_assets($file){
  $str = 'https://s3.amazonaws.com/mazkaracdn/';
  return $str.$file;
}

