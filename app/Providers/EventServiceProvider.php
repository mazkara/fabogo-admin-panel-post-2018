<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        // 'App\Events\MerchantCreated' => [
            // 'App\Listeners\Merchant\SetAttributables',
        // ],


        'App\Events\BusinessCreated' => [
            'App\Listeners\SetAttributables',
        ],
        'App\Events\BusinessUpdated' => [
            'App\Listeners\SetAttributables',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
