<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;




class Zone extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_zone';
  }

  public static $rules = [];

  public $fields = array('description','depth','name','slug','active','business_count','latitude','longitude','old_id','city_id','parent_id');

  public $fillables = array('description','depth','name','slug','active','business_count','latitude','longitude','old_id','city_id','parent_id');
  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function city(){
    return $this->belongsTo('App\Models\City');
  }

}
