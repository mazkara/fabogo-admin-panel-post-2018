<?php
namespace App\Models;


use Eloquent;
use App\Models\Virtual_number_allocation;

class Virtual_number extends Eloquent {
  protected $guarded = array();
  public function __construct(){
    parent::__construct();
    $this->table = 'api_virtualnumber';
  }


  protected $fillable = [ 'body','city_id'];

  public static $rules = array(
    'body' => 'required|unique:api_virtualnumber',
    //'state' => 'required'
  );

  public static $edit_rules = array(
    'body' => 'required|unique:api_virtualnumber',
    //'state' => 'required'
  );

  public function virtual_number_allocations(){
    return $this->hasMany('App\Models\Virtual_number_allocation', 'virtual_number_id');
  }

  public function current_virtual_number_allocations(){
    return $this->virtual_number_allocations()->where('api_virtualnumberallocation.state', '=', 'active')->first();
  }

  public function scopeOnlyAllocatable($query){
    $allocated_numbers = Virtual_number_allocation::where('state', '=', 'active')->get()->pluck('virtual_number_id', 'virtual_number_id')->all();
    return $query->whereNotIn('id', $allocated_numbers);
  }

  public function scopeByBody($query, $body){
    return $query->where('body', '=', $body);
  }

  public function scopeBySearch($query, $body){
    return $query->where('body', 'LIKE', '%'.$body.'%');
  }

  public function current_business(){
    $c = $this->current_virtual_number_allocations();
    if(!is_null($c)){
      return $c->business()->first();
    }

    return false;
  }

  public function current_business_name(){
    $c = $this->current_business();
    if(!($c)){
      return '<em>None</em>';
    }

    return $c->name;
  }

  public function city(){
    return $this->belongsTo('App\Models\City', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function scopeOnlyActive($query){
    return $query->where('state', '=', 'active');
  }

  public function deactivate(){
    $this->state = 'inactive';
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }

}
