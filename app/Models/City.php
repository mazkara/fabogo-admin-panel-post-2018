<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;


class City extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_city';
  }

  public static $rules = [];

  public $fields = array('name','slug');

  public $fillables = array('name','slug');


}
