<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;



class Group extends Eloquent{
  protected $guarded = array();
  use Sluggable;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }


  public function __construct(){
    parent::__construct();
    $this->table = 'api_group';
  }


  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return  $query->where('city_id', '=', $locale);
  }

  public function scopeByType($query, $type){
    return  $query->where('type', '=', $type);
  }

  public static $rules = [
    'description'=>"required",
    'name'=>"required"
  ];

  public static $fields = array('description','created_at','updated_at','name','type','slug','is_custom_active','pre_side_html','old_id','city_id','image_id');

  public static  $fillables = array('description','created_at','updated_at','name','type','slug','is_custom_active','pre_side_html','old_id','city_id','image_id');

  public function businesses(){
    return $this->hasMany('\App\Models\Business', 'chain_id');
  }

  public function image(){
    return $this->belongsTo('\App\Models\Image', 'image_id');
  }


  public function savePhoto($photo){
    $name = $photo->getClientOriginalName();
    $image_path = $photo->getPathName();
    $image_large = \Image::make($image_path)->resize(1200, null, function ($constraint) {
      $constraint->aspectRatio();
    });

    $image_thumbnail = \Image::make($image_path)->fit(400, 275);

    $image_large = $image_large->stream();
    $image_thumbnail = $image_thumbnail->stream();

    $folder = '/images/'.$this->id.'_'.time();

    $large_path = $folder.'/large/'.$name;
    $thumb_path = $folder.'/medium/'.$name;

    \Storage::disk('s3')->put($large_path, $image_large->__toString(), 'public');
    \Storage::disk('s3')->put($thumb_path, $image_thumbnail->__toString(), 'public');

    $foto = $this->image()->create(array(
      'description'=>' image for group '.$this->id,
      'name'=>$name,
      'image_url'=>'https://s3.amazonaws.com/mazkaracdn'.$large_path,
      'image_thumbnail_url'=>'https://s3.amazonaws.com/mazkaracdn'.$thumb_path,
      'imagable_id'=>$this->id,
      'image_type'=>'Chain',
      'image_file_size'=>0,
      'slot'=>0
    ));

    $this->image_id = $foto->id;
    $this->save();
  }

  public function removePhoto(){
    $this->image_id = null;
    $this->save();
  }




}
