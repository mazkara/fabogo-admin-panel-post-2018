<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


class Module extends Eloquent
{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_module';
  }

  public static $fields = array( 'description','name','icon_name','is_default');

  public function module_accesses(){
    return $this->hasMany('App\Models\Module_access');
  }

  public function businesses(){
    return $this->hasManyThrough('App\Models\Business', 'App\Models\Module_access');
  }



}
