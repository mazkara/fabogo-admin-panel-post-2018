<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;


class Highlight extends Eloquent{
  protected $guarded = array();
  use Sluggable;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }


  public function __construct(){
    parent::__construct();
    $this->table = 'api_highlights';
  }

  public static $rules = [
    'description'=>'required',
    'name'=>'required',
    'state'=>'required',
  ];

  public $fields = array('description','created_at','updated_at','name','slug','css','business_count','state','old_id',);

  public $fillables = array('description','created_at','updated_at','name','slug','css','business_count','state','old_id',);

  public function isActive(){
    return $this->state == 'active' ? true : false;
  }

}
