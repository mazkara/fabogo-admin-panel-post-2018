<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;

class Call_log extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_calllog';
  }

  public static $rules = [];

  public $fields = array('description', 'dated', 'timed', 'called_number', 'caller_number', 'caller_duration', 'agent_list', 'call_connected_to', 'call_transfer_status', 'call_transfer_duration', 'call_recording_url', 'call_start_time', 'call_pickup_time', 'caller_circle', 'business_id', 'is_downloaded', 'is_lead', 'state', 'is_ppl', 'media_file_name', 'media_file_size', 'media_content_type', 'media_updated_at', 'invoice_item_id', 'type_of_call');

  public $fillables = array('description', 'dated', 'timed', 'called_number', 'caller_number', 'caller_duration', 'agent_list', 'call_connected_to', 'call_transfer_status', 'call_transfer_duration', 'call_recording_url', 'call_start_time', 'call_pickup_time', 'caller_circle', 'business_id', 'is_downloaded', 'is_lead', 'state', 'is_ppl', 'media_file_name', 'media_file_size', 'media_content_type', 'media_updated_at', 'invoice_item_id', 'type_of_call');
}
