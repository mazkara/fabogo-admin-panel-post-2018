<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;




class Business_details extends Eloquent
{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_businessdetails';
  }

  public static $rules = [];


  public function business($query){
    return $this->belongsTo('\App\Models\Business', 'business_id');
  }

  public static  $fields = array(
                'description','website','facebook','twitter','instagram','google',
                'landmark','rate_card_count','image_count','reviews_count',
                'favorites_count','checkins_count','services_count','categories_count',
                'highlights_count','timings_count','meta','active_packages_count',
                'total_ratings_count','cost_estimate','active_offers_count','ref',
                'has_sample_menu','has_stock_cover_image','type','is_featured','is_rich',
                'cover_x_pos','cover_y_pos','deal_purchase_count','booking_slot_minute', 'preferred_email', 'preferred_phone',
                'business_id','popularity','about','display_json'
              );

  public static  $fillables = array(
                'description','website','facebook','twitter','instagram','google',
                'landmark','rate_card_count','image_count','reviews_count',
                'favorites_count','checkins_count','services_count','categories_count',
                'highlights_count','timings_count','meta','active_packages_count',
                'total_ratings_count','cost_estimate','active_offers_count','ref',
                'has_sample_menu','has_stock_cover_image','type','is_featured','is_rich',
                'cover_x_pos','cover_y_pos','deal_purchase_count','booking_slot_minute','preferred_email', 'preferred_phone',
                'business_id','popularity','about','display_json'
              );
  protected  $fillable = array(
                'description','website','facebook','twitter','instagram','google',
                'landmark','rate_card_count','image_count','reviews_count',
                'favorites_count','checkins_count','services_count','categories_count',
                'highlights_count','timings_count','meta','active_packages_count',
                'total_ratings_count','cost_estimate','active_offers_count','ref',
                'has_sample_menu','has_stock_cover_image','type','is_featured','is_rich',
                'cover_x_pos','cover_y_pos','deal_purchase_count','booking_slot_minute','preferred_email', 'preferred_phone',
                'business_id','popularity','about','display_json'
              );

  public function getOffersAttribute(){
    $json = json_decode($this->display_json);

    if(!is_object($json)){
      return [];
    }

    if(!isset($json->top_offers_array)){
      return [];
    }

    $offers = is_array($json->top_offers_array)?$json->top_offers_array:[];
    return $offers;
  }


  public function setOffersAttribute($offers){
    $json = array('top_offers_array'=>array());
    foreach($offers as $offer){
      $json['top_offers_array'][] = array('title'=>$offer['title'], 'tag'=>$offer['tag']);
    }

    $this->display_json = json_encode($json);
  }




}
