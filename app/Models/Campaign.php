<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

use App\Models\Zone;

class Campaign extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_campaign';
  }

  public static $rules = [
    'description'=>'required',
    'start_date' => 'required|date_format:Y-m-d|before:end_date',
    'end_date' => 'required|date_format:Y-m-d|after:start_date',
    'merchant_id'=>'required'
  ];

  public static $fields = array('description','merchant_id','start_date','end_date');
  protected $fillable = array('description','merchant_id','start_date','end_date');
}
