<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    
    protected $primaryKey = "id";
    
    public function __construct(){
        parent::__construct();
        $this->table = 'api_user';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function hasTheRoles($roles){
    $roles = is_array($roles)?$roles:[$roles];

    $current_roles = explode(',', $this->role);
    $intersecting_roles = array_intersect($roles, $current_roles);

    return count($intersecting_roles) > 0 ? true : false;
  }

   public function detachTheRoles($roles){
    $roles = is_array($roles)?$roles:[$roles];

    $current_roles = explode(',', $this->role);

    foreach($roles as $role){
        $pos = array_search($role, $current_roles);
        if ($pos !== false) unset($current_roles[$pos]);
    }

    $this->role = join(',', $current_roles);
    $this->save();
  }

   public function attachTheRoles($roles){
    $roles = is_array($roles)?$roles:[$roles];

    $current_roles = explode(',', $this->role);
    $this->role = join(',', array_unique(array_merge($current_roles, $roles)));

    $this->save();
  }

  public function getSelectableFullNameAttribute(){
    return $this->id.' - '.(empty($this->name)?$this->username:$this->name).'('.$this->email.')';
  }


  public function scopeByName($query, $search){
    return $query->where('name', 'like', $search.'%')
                ->orWhere('email', 'like', $search.'%');
  }


  public function scopeByRoles($query, $roles){
    $sql = [];

    foreach($roles as $role){
      $sql[] = "(role like '%".$role."%')";
    }

    return $query->whereRaw('('. join('OR', $sql) .')');
  }



  public function scopeSearch($query, $search){
    return $query->where('name', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%');
  }

  public function getGravatarAttribute($size = 80){
    $meta_photo = $this->getMeta('photo-micro');
    if($meta_photo!=null){
      return $meta_photo;
    }
    if(count($this->avatar)>0){
      return $this->avatar->image->url('small');

    }

    //elseif(count($this->accounts)>0){
    //  $account = $this->accounts()->where('provider', '=', 'facebook')->get();
    //  if(count($account)>0){
    //    $account = $account->first();
    //    return 'http://avatars.io/facebook/'.$account->provider_id;
    //  }
    //}
    $hash = md5(strtolower(trim($this->attributes['email'])));
    return "http://www.gravatar.com/avatar/$hash?s=".$size."&d=identicon";//.urlencode(asset('assets/avatar.jpg'));
  }

}
    