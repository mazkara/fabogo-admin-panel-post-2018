<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


class Timing extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_timings';
  }

  public static $rules = [
  ];

  public static $fields = array();
}
