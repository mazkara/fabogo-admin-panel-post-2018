<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


class Image extends Eloquent {
// implements StaplerableInterface

  protected $guarded = array();
  protected $fillable = [ 'description','name','image_url','image_thumbnail_url',
                          'imagable_id','image_type','image_file_size',
                          'business_id','service_id','slot'];

  public static $rules = [];

  public function __construct(array $attributes = array()) {
    $this->table = 'api_image';
    parent::__construct($attributes);
  }

  public function business(){
    return $this->belongsTo('\App\Models\Business');
  }

  public function getImageThumbnailPathAttribute(){
    return (strstr($this->image_thumbnail_url, 'http') ? '' :'https://s3.amazonaws.com/mazkaracdn').$this->image_thumbnail_url;
  }

  public function getImagePathAttribute(){
    return (strstr($this->image_url, 'http') ? '' :'https://s3.amazonaws.com/mazkaracdn').$this->image_url;
  }

  public function isCover(){
    return $this->image_type == 'cover-image' ? true : false;
  }


  public function upload($file){
    \Image::make($file,array(
      'width' => 300,
      'height' => 300,
      'grayscale' => true
    ))->save('/path/to/the/thumbnail.jpg');
  }

}
