<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Validator;

class WikisController extends Controller {

  /**
   * Brandadvert Repository
   *
   * @var Brandadvert
   */
  protected $wiki;

  public function __construct(\App\Models\Wiki $wiki)
  {
    $this->wiki = $wiki;
    $this->layout = 'layouts.admin-crm';    
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(){
    $wikis = $this->wiki->query();
    $services = \App\Models\Service::all();
    $params = Input::all();

    if(isset($params['search']) && !empty($params['search'])){
      $wikis = $wikis->where('description', 'like', '%'.$params['search'].'%')
                    ->orWhere('title', 'like', '%'.$params['search'].'%')
                    ->orWhere('service_details', 'like', '%'.$params['search'].'%');
    }

    if(isset($params['service_id']) && !empty($params['service_id'])){
      $wikis = $wikis->where('service_id', '=', $params['service_id']);
    }

    $wikis = $wikis->orderby('id', 'desc')->paginate(20);
    return view('wikis.index', compact('wikis', 'services', 'params'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $excludables = \App\Models\Wiki::select()->get()->pluck('service_id', 'service_id')->all();
    $services = \App\Models\Service::select()->whereNotIn('id', $excludables)->where('parent_id', '>', 0)->get()->pluck('name', 'id')->all();

    return view('wikis.create', compact('services'));
  }


  public function store()
  {
    $input = Input::all();
    //dump($input);die();
    $validation = Validator::make($input, \App\Models\Wiki::$rules);

    if ($validation->passes()){
      $data = Input::all();
      $wiki = new \App\Models\Wiki();
      $wiki->description = $data['description'];
      $wiki->title = $data['title'];
      $wiki->service_details = $data['service_details'];
      $display_json = array();

      if(isset($data['display_json']) && isset($data['display_json']['data_array'])){
        foreach($data['display_json']['data_array'] as $js){
          $display_json[] = $js;
        }
      }

      $data['display_json']['data_array'] = $display_json;

      $wiki->display_json = json_encode($data['display_json']);
      $wiki->active = $data['active'];
      $wiki->service_id = $data['service_id'];
      $wiki->cover_image_id = $data['cover_image_id'];
      $wiki->save();

      return redirect()->route('admin.wikis.index');
    }
    return redirect()->back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $wiki = $this->wiki->findOrFail($id);
    return view('wikis.show', compact('wiki'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id){
    $wiki = $this->wiki->find($id);
    $excludables = \App\Models\Wiki::select()->get()->pluck('service_id', 'service_id')->all();
    unset($excludables[$wiki->service_id]);
    $services = \App\Models\Service::select()->whereNotIn('id', $excludables)->where('parent_id', '>', 0)->get()->pluck('name', 'id')->all();

    if (is_null($wiki)){
      return redirect()->route('admin.wikis.index');
    }

    return view('wikis.edit', compact('wiki', 'services'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, \App\Models\Wiki::$rules);

    if ($validation->passes()){
      $data = Input::all();
      $wiki = \App\Models\Wiki::find($id);

      $wiki->description = $data['description'];
      $wiki->title = $data['title'];
      $wiki->service_details = $data['service_details'];
      $display_json = array();
      if(isset($data['display_json']) && isset($data['display_json']['data_array'])){
        foreach($data['display_json']['data_array'] as $js){
          $display_json[] = $js;
        }
      }
      $data['display_json']['data_array'] = $display_json;
      $wiki->display_json = json_encode($data['display_json']);
      $wiki->active = $data['active'];
      $wiki->service_id = $data['service_id'];
      $wiki->cover_image_id = $data['cover_image_id'];
      $wiki->save();

      return redirect()->route('admin.wikis.index');
    }

    return redirect()->back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $wiki = $this->wiki->find($id);

      if(!$wiki){
        return redirect()->back()->with('warning', 'The wiki has been deleted');
      }

      $wiki->delete();

    return redirect()->back();
  }

}
