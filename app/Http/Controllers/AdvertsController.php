<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class AdvertsController extends Controller {

  /**
   * Brandadvert Repository
   *
   * @var Brandadvert
   */
  protected $advert;

  public function __construct(\App\Models\Advert $advert)
  {
    $this->advert = $advert;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $adverts = $this->advert->query();
    $merchants = \App\Models\Merchant::select()->get()->pluck('name', 'id')->all();
    $params = Input::all();

    $adverts = $adverts->orderby('id', 'desc')->paginate(20);
    return view('adverts.index', compact('adverts', 'merchants', 'params'));
  }


  public function updatable(){
    $input = Input::all();

    $data = ['id' => $input['pk']];

    $advert = $this->advert->find($data['id']);

    $campaign = \App\Models\Campaign::find($advert->campaign_id);

    if($input['name'] == 'start_date'){
      if(strtotime($input['value']) < strtotime($campaign->start_date)){
        //error
        return response('Error! Start date cannot be earlier than campaign start date', 400);

      }elseif(strtotime($input['value']) > strtotime($advert->end_date)){
        return response('Error! Start date cannot be later than ad end date', 400);
      }else{
        $advert->start_date = $input['value'];
        $advert->save();

      }
    }elseif($input['name'] == 'end_date'){
      if(strtotime($input['value']) > strtotime($campaign->end_date)){
        //error
        return response('Error! End date cannot be later than campaign end date', 400);
      }elseif(strtotime($input['value']) < strtotime($advert->start_date)){
        return response('Error! End date cannot be earlier than ad start date', 400);
      }else{
        $advert->end_date = $input['value'];
        $advert->save();
      }
    }

    return response('Success', 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $campaign_id = Input::get('campaign_id');
    $advert_campaign = \App\Models\Campaign::find($campaign_id);
    $merchant = \App\Models\Merchant::find($advert_campaign->merchant_id);
    if(!$merchant){
      $merchant = \App\Models\Merchant::select()->first();
    }

    $businesses = \App\Models\Business::select()->where('merchant_id', '=', $merchant->id)->get()->pluck('displayable', 'id')->all();

    $services = \App\Models\Service::select()->where('parent_id', '>', 0)->get()->pluck('name', 'id')->all();
    $zones = \App\Models\Zone::select()->where('depth', '>', 1)->get()->pluck('name', 'id')->all();

    $merchants = \App\Models\Merchant::select()->orderby('name', 'asc')->get()->pluck('displayable', 'id')->all();

    return view('adverts.create', compact('merchant', 'businesses', 'advert_campaign', 'services', 'zones'));
  }

  protected static $slots = [ 1 =>'Slot 1', 2 =>'Slot 2', 3 =>'Slot 3', 4 =>'Slot 4', 5 =>'Slot 5', 
                              6 =>'Slot 6', 7 =>'Slot 7', 8 =>'Slot 8', 9 =>'Slot 9', 10 =>'Slot 10',
                              11 =>'Slot 11', 12 =>'Slot 12', 13 =>'Slot 13', 14 =>'Slot 14', 15 =>'Slot 15'
                            ];


  public function getAvailableSlots(){
    $inputs = Input::all();
    $chk = count($inputs['service_ids']) + count($inputs['zone_ids']);


    if(($chk ==0) && empty($inputs['end_date']) && empty($inputs['start_date'])){
      return response()->json([]);
    }

    $exclude = $inputs['exclude'];
    $slots = Self::$slots;



    $booked_slots = \App\Models\Advert::select('slot')->where('id', '<>', $exclude)
                        ->whereIn('service_id', $inputs['service_ids'])
                        ->whereIn('zone_id', $inputs['zone_ids'])
                        ->where('start_date', '<=', $inputs['start_date'])
                        ->where('end_date', '>=', $inputs['end_date'])->get();

    foreach($booked_slots as $b){
      unset($slots[$b['slot']]);
    }

    return response()->json($slots);
  }




  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $input = Input::all();
    //dump($input);die();
    $validation = Validator::make($input, \App\Models\Advert::$rules);

    if ($validation->passes())
    {
      $data = Input::all();

      foreach($data['service_ids'] as $service_id){
        foreach($data['zone_ids'] as $zone_id){

          $advert = new \App\Models\Advert();

          $advert->description = $data['description'];
          $advert->slot = $data['slot'];
          $advert->start_date = $data['start_date'];
          $advert->end_date = $data['end_date'];
          $advert->business_id = $data['business_id'];
          $advert->campaign_id = $data['campaign_id'];
          $advert->service_id = $service_id;
          $advert->zone_id = $zone_id;
          $advert->save();


        }
      }

      return redirect()->route('admin.campaigns.show', $data['campaign_id']);
    }

    return redirect()->back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $advert = $this->advert->findOrFail($id);
    return view('adverts.show', compact('advert'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $advert = $this->advert->find($id);
    $campaign_id = $advert->campaign_id;
    $advert_campaign = \App\Models\Campaign::find($campaign_id);
    $merchant = \App\Models\Merchant::find($advert_campaign->merchant_id);
    if(!$merchant){
      $merchant = \App\Models\Merchant::select()->first();
    }

    $businesses = \App\Models\Business::select()->where('merchant_id', '=', $merchant->id)->get()->pluck('displayable', 'id')->all();

    $services = \App\Models\Service::select()->where('parent_id', '>', 0)->get()->pluck('name', 'id')->all();
    $zones = \App\Models\Zone::select()->where('depth', '>', 1)->get()->pluck('name', 'id')->all();

    $merchants = \App\Models\Merchant::select()->orderby('name', 'asc')->get()->pluck('displayable', 'id')->all();


    if (is_null($advert)){
      return redirect()->route('admin.adverts.index');
    }

    return view('adverts.edit', compact('advert','merchant', 'businesses', 'advert_campaign', 'services', 'zones'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, \App\Models\Advert::$rules);

    if ($validation->passes())
    {
      $advert = $this->advert->find($id);

      $data = Input::only(\App\Models\Advert::$fields);
      //$data['uploaded_by'] = \Auth::user()->id;
      unset($data['city_id']);

      $advert->update($data);

      return redirect()->route('admin.adverts.show', $id);
    }

    return redirect()->route('admin.adverts.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */

  public function deleteOne($id){
    $ad = $this->advert->find($id);

    if(!$ad){
      return redirect()->back()->with('warning', 'The advert has already been deleted');
    }

    $ad->delete();
    return redirect()->back();
  }

  public function destroy($id)
  {
    $ids = explode(',', $id);
    foreach($ids as $one_id){
      $ad = $this->advert->find($one_id);

      if(!$ad){
        return redirect()->back()->with('warning', 'The advert has already been deleted');
      }

      $ad->delete();
    }

    return redirect()->back();
  }

}
