<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class CategoriesController extends Controller {

	/**
	 * Category Repository
	 *
	 * @var Category
	 */
	protected $category;

	public function __construct(\App\Models\Category $category)
	{
		$this->category = $category;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = $this->category->all();

		//$this->layout = view('layouts.admin');
    return view('categories.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    return view('categories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, \App\Models\Category::$rules);
		$data = Input::only(\App\Models\Category::$fields);

		if ($validation->passes()){
			$category = $this->category->create($data);
			return redirect()->route('admin.categories.index');
		}

		return redirect()->route('admin.categories.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = $this->category->findOrFail($id);

    return view('categories.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = $this->category->find($id);

		if (is_null($category)){
			return redirect()->route('admin.categories.index');
		}

    return view('categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$input = array_except(Input::except('images'), '_method' );
		$validation = Validator::make($input, \App\Models\Category::$rules);
		$data = Input::only(\App\Models\Category::$fields);

		if ($validation->passes()){
			$category = $this->category->find($id);
			$category->update($data);
			return redirect()->route('admin.categories.index', $id);
		}

		return redirect()->route('admin.categories.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return redirect()->route('admin.categories.index');
	}

}
