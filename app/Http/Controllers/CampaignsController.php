<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class CampaignsController extends Controller {

  /**
   * Brandadvert_campaign Repository
   *
   * @var Brandadvert_campaign
   */
  protected $advert_campaign;

  public function __construct(\App\Models\Campaign $advert_campaign)
  {
    $this->advert_campaign = $advert_campaign;
    $this->layout = 'layouts.admin-crm';    
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $advert_campaigns = $this->advert_campaign->query();

    $params = Input::all();

    if(isset($params['search']) && !empty($params['search'])){
      $advert_campaigns = $advert_campaigns->where('description', 'like', '%'.$params['search'].'%');
    }

    if(isset($params['merchant_id']) && !empty($params['merchant_id'])){
      $advert_campaigns = $advert_campaigns->where('merchant_id', '=', $params['merchant_id']);
    }


    // $merchant_ids = $advert_campaigns->pluck('merchant_id','merchant_id')->all();

    $merchants = \App\Models\Merchant::select()->byLocale()->get()->pluck('name', 'id')->all();
    $merchant_ids = \App\Models\Merchant::select()->byLocale()->get()->pluck('id', 'id')->all();

    $advert_campaigns = $advert_campaigns->whereIn('merchant_id', $merchant_ids)->orderby('id', 'desc')->paginate(20);

    //->whereIn('id', $merchant_ids)->get()->lists('name', 'id')->all();



    return view('campaigns.index', compact('advert_campaigns', 'merchants', 'params'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $merchants = \App\Models\Merchant::select()->byLocale()->orderby('name', 'asc')->get()->pluck('displayable', 'id')->all();
    return view('campaigns.create', compact('merchants'));
  }



  public function store()
  {
    $input = Input::all();
    $validation = Validator::make($input, \App\Models\Campaign::$rules);

    if ($validation->passes())
    {
      $data = Input::only(\App\Models\Campaign::$fields);
      $advert_campaign = new \App\Models\Campaign();
      foreach($data as $i=>$v){
        $advert_campaign->$i = $v;
      }

      //$advert_campaign = $this->advert_campaign->create($data);
      $advert_campaign->save();
      return redirect(route('admin.campaigns.index'));
    }

    return redirect(route('admin.campaigns.create'))
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $advert_campaign = $this->advert_campaign->findOrFail($id);
    $adverts = \App\Models\Advert::select()->where('campaign_id', '=', $id)->get();
    $adverts = \App\Models\Advert::select(
                  \DB::raw('*,  group_concat(slot) as slots, 
                                group_concat(id) as ids, 
                                group_concat(service_id) as service_ids, 
                                group_concat(zone_id) as zone_ids'))
                    ->where('campaign_id', '=', $id)
                    ->groupBy('start_date', 'end_date', 'slot')->get();


    $advertz = \App\Models\Advert::select()->where('campaign_id', '=', $id)->get();

    $venue_ids = $advertz->pluck('business_id','business_id')->all();
    $zone_ids = $advertz->pluck('zone_id','zone_id')->all();
    $service_ids = $advertz->pluck('service_id','service_id')->all();

    $zones = \App\Models\Zone::select()->whereIn('id', $zone_ids)->pluck('name', 'id')->all();
    $services = \App\Models\Service::select()->whereIn('id', $service_ids)->pluck('name', 'id')->all();
    $venues = \App\Models\Business::select()->whereIn('id', $venue_ids)->pluck('name', 'id')->all();

    return view('campaigns.show', compact('advert_campaign', 'adverts','zones','services','venues'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $advert_campaign = $this->advert_campaign->find($id);
    $merchants = \App\Models\Merchant::select()->byLocale()->orderby('name', 'asc')->get()->pluck('displayable', 'id')->all();

    $last_date_advert = \App\Models\Advert::where('campaign_id', '=', $id)->orderby('end_date', 'desc')->first();
    $first_date_advert = \App\Models\Advert::where('campaign_id', '=', $id)->orderby('start_date', 'asc')->first();

    $start_at = $first_date_advert->start_date;
    $end_at = $first_date_advert->end_date;
    if (is_null($advert_campaign)){
      return redirect(route('admin.campaigns.index'));
    }

    return view('campaigns.edit', compact('merchants', 'start_at', 'end_at', 'advert_campaign'));
  }

  public function getEditableAverts($id){
    $advert_campaign = $this->advert_campaign->find($id);
    $merchants = \App\Models\Merchant::select()->byLocale()->orderby('name', 'asc')->get()->pluck('displayable', 'id')->all();

    $last_date_advert = \App\Models\Advert::where('campaign_id', '=', $id)->orderby('end_date', 'desc')->first();
    $first_date_advert = \App\Models\Advert::where('campaign_id', '=', $id)->orderby('start_date', 'asc')->first();

    if(!($first_date_advert) && !($first_date_advert)){
      return redirect()->back();
    }

    $start_at = $first_date_advert->start_date;
    $end_at = $first_date_advert->end_date;


    $advertz = \App\Models\Advert::select()->where('campaign_id', '=', $id)->get();

    $venue_ids = $advertz->pluck('business_id','business_id')->all();
    $zone_ids = $advertz->pluck('zone_id','zone_id')->all();
    $service_ids = $advertz->pluck('service_id','service_id')->all();

    $adverts = $advertz->all();

    $zones = \App\Models\Zone::select()->whereIn('id', $zone_ids)->pluck('name', 'id')->all();
    $services = \App\Models\Service::select()->whereIn('id', $service_ids)->pluck('name', 'id')->all();
    $venues = \App\Models\Business::select()->whereIn('id', $venue_ids)->pluck('name', 'id')->all();


    if (is_null($advert_campaign)){
      return redirect(route('admin.campaigns.index'));
    }

    return view('campaigns.edit-allocated', compact('adverts','zones','services','venues','merchants', 'start_at', 'end_at', 'advert_campaign'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, \App\Models\Campaign::$rules);

    if ($validation->passes())
    {
      $advert_campaign = $this->advert_campaign->find($id);

      $data = Input::only(\App\Models\Campaign::$fields);

      $advert_campaign->update($data);

      return redirect(route('admin.campaigns.show', $id));
    }

    return redirect(route('admin.campaigns.edit', $id))
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $advert_campaign = $this->advert_campaign->find($id);
    if(!$advert_campaign){
      return redirect()->back()->with('warning', 'The advert_campaign has already been deleted');
    }

    \App\Models\Advert::where('campaign_id', '=', $id)->delete();

    $advert_campaign->delete();

    return redirect(route('admin.campaigns.index'));
  }

}
