<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Input;
use Validator;

class MerchantsController extends Controller {

  /**
   * Merchant Repository
   *
   * @var Merchant
   */
  protected $merchant;

  public function __construct( \App\Models\Merchant $merchant)
  {
    $this->merchant = $merchant;
  }


  public function export(){

    $input = Input::all();

    $nm = 'Fabogo_Merchants_for_'.MazkaraHelper::getAdminLocale().'_'.time();

Excel::create($nm, function($excel) {

    $excel->sheet('Merchants', function($sheet) {
    $input = Input::all();

    //$skip = 1000*$input['page'];


    $columns = ['merchant', 'venue_name', 'current_acd'];
    $data = [];

    $merchants = Merchant::select()->byLocale()->get();
    foreach($merchants as $merchant){
      
      foreach($merchant->businesses as $business){
        $row = array();
        $row['merchant'] = $merchant->name;
        $row['venue_name'] = $business->name;
        $row['venue_id'] = $business->id;
        $row['current_acd'] = $business->isACDPhoneAllocated();
        $data[] = $row;
      }
    }


        $sheet->fromArray($data);

    });

})->export('csv');
  }



  public function getUsersForMerchant(){
    $input = Input::all();
    $users = \App\Models\User::byName($input['term'])->get()->toArray();
    foreach($users as $ii=>$user){
      $html = view('merchants.partials.user', ['user' => $user]);
      $users[$ii]['html'] = (string) $html;
    }

    return response()->json($users);
  }

  public function getOutletsForMerchant(){
    $input = Input::all();
    $businesses = \App\Models\Business::query()->searchBasic($input['term'])->allocatableToMerchants()->get()->take(10)->toArray();
    foreach($businesses as $ii=>$business){
      //$business['preferred_email'] = join(',', $business['email']);
      $html = view('merchants.partials.business', ['business' => $business]);
      $businesses[$ii]['html'] = (string) $html;
    }

    return response()->json($businesses);
  }

  public function getOutletsForMerchantInvoiceForm(){
    $input = Input::all();
    $merchant = \App\Models\Merchant::find($input['merchant_id']);

    $result = array();
    $view = view('merchants.partials.business-for-invoice-form', ['merchant' => $merchant]);
    $result['html'] = $view->render();
    
    return response()->json($result);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */

  public function globalIndex(){
    $all_merchants = $this->getGlobalListOfMerchants();
    return view('merchants.global', compact('params', 'all_merchants'));
  }

  public function getFormForModuleAccess($merchant_id, $user_id){
    $merchant = \App\Models\Merchant::find($merchant_id);
    $user = \App\Models\User::find($user_id);

    $merchant_user = \DB::table('merchant_user')->where('merchant_id', '=', $merchant_id)
                              ->where('user_id', '=', $user_id)
                              ->where('role', '=', 'merchant')->first();

    $modules = Merchant_module::all();
    $access = Merchant_module_access::select()->where('merchantUser_id', '=', $merchant_user->id)->get();
    $modules_access = array();
    foreach($modules as $module){
      $modules_access[$module->id] = false;
    }

    foreach($access as $one_access){  
      if(isset($modules_access[$one_access->module_id])){
        $modules_access[$one_access->module_id] = $one_access;
      }
    }

    $result = array();
    $view = view('merchants.partials.module-access-form', compact('modules_access', 'merchant', 'user', 'merchant_user', 'modules', 'access'));
    $result['html'] = $view->render();
    
    return response()->json($result);
  }

  function postModuleAccess(){
    $input =  Input::all();

    foreach($input['modules'] as $module_data){
      if($module_data['id'] == 0){
        unset($module_data['id']);
        Merchant_module_access::create($module_data);
      }else{
        $merchant_module_access = Merchant_module_access::find($module_data['id']);
        $merchant_module_access->update($module_data);
      }
    }

    return redirect()->back();
  }

  public function exportGlobalIndex(){

    $nm = 'Global_Merchants_'.time();

Excel::create($nm, function($excel) {

    $excel->sheet('Global Merchants', function($sheet) {
    $input = Input::all();

    //$skip = 1000*$input['page'];


    $columns = ['global_id','local_id','name','tan','phone','email','location','bigreach','fabogo'];
    $data = [];

    $all_merchants = $this->getGlobalListOfMerchants();
    foreach($all_merchants as $merchant){
      
        $row = array();
        $row['global_id'] = $merchant['global_id'];
        $row['local_id'] = $merchant['id'];
        $row['name'] = $merchant['name'];
        $row['tan'] = $merchant['tan'];
        $row['phone'] = $merchant['phone'];
        $row['email'] = $merchant['email'];
        $row['location'] = $merchant['city_name'];
        $row['bigreach'] = $merchant['is_bigreach']?'true':'false';
        $row['fabogo'] = $merchant['is_fabogo']?'true':'false';

        $data[] = $row;
    }


        $sheet->fromArray($data);

    });

})->export('csv');

  }


  public function index(){
    $merchants = $this->merchant->query()->byLocale();

    $params = [];
    $input =  Input::all();
    if(Input::has('search') && ($input['search']!='')){
      $merchants->bySearch($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('phone') && ($input['phone']!='')){
      $merchants->byVirtualNumber($input['phone']);
      $params['phone'] = $input['phone'];
    }

    $merchants = $merchants->orderby('id', 'desc')->paginate(20);

    //$this->layout = view('layouts.admin');
    return view('merchants.index', compact('params', 'merchants'));
  }

  private function getGlobalListOfMerchants(){
        $merchants = DB::table('merchants')
            ->join('zones', 'merchants.city_id', '=', 'zones.id')
            ->select('merchants.*', 'zones.name as city_name')
            ->get();
    $merchants = json_decode(json_encode($merchants), true);

    $fabogo_merchants = [];
    $client = new \GuzzleHttp\Client();
    $url = 'http://admin.bigreach.io/api/merchants';
    $res = $client->request('GET', $url);
    $response = $res->getBody();
    $bigreach_merchants = json_decode($response, TRUE);
    $all_merchants = [];
    foreach($merchants as $merchant){
      $merchant['is_fabogo'] = true;
      if(isset($bigreach_merchants[$merchant['global_id']])){
        $merchant['is_bigreach'] = true;
        unset($bigreach_merchants[$merchant['global_id']]);
      }else{
        $merchant['is_bigreach'] = false;
      }
      $all_merchants[$merchant['global_id']] = $merchant;
    }

    foreach($bigreach_merchants as $merchant){
      $merchant['is_fabogo'] = false;
      $merchant['is_bigreach'] = true;
      $all_merchants[$merchant['global_id']] = $merchant;
    }
    return $all_merchants;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create(){
    //$this->layout = view('layouts.admin');
    return view('merchants.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $input = Input::only(\App\Models\Merchant::$fields);
    $validation = Validator::make($input, \App\Models\Merchant::$rules);
    $inp = Input::all();

    if ($validation->passes()){
      $merchant = $this->merchant->create($input);
      $merchant->update($input);
      $merchant->setCity();
      $input = Input::only('businesses', 'users', 'sales_admins');

      $users = isset($input['users']) && is_array($input['users'])?$input['users']:[];
      $_businesses = isset($inp['businesses']) && is_array($inp['businesses'])?$inp['businesses']:[];

      $merchant->allocateUsersNPocs($users, $input['sales_admins']);
      /*if(count($_businesses)>0){
        $businesses =  array();
        foreach($_businesses as $ii=>$vv){
          $businesses[$vv] = array('preferred_email'=>$inp['business_emails'][$ii]);
        }
      }*/

      $merchant->generateGlobalID();
      $merchant->allocateBusinesses($_businesses);
      $merchant->save();

      return redirect()->route('admin.merchants.index');
    }

    return redirect()->route('admin.merchants.create')
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  public function toggleLeadsAccessForBusiness($id){
    $business = \App\Models\Business::find($id);
    $business->toggleAccessLeadsReport();
    $business->save();
    return redirect()->back()->with('notice', 'Lead access has been updated');
  }

  public function toggleResumesAccessForBusiness($id){
    $business = \App\Models\Business::find($id);
    $business->toggleAccessResumesReport();
    $business->save();
    return redirect()->back()->with('notice', 'Resume access has been updated');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $merchant = $this->merchant->findOrFail($id);

    //$this->layout = view('layouts.admin');
    return view('merchants.show', compact('merchant'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $merchant = $this->merchant->find($id);

    if (is_null($merchant))
    {
      return redirect()->route('admin.merchants.index');
    }

    //$this->layout = view('layouts.admin');
    return view('merchants.edit', compact('merchant'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = Input::only(\App\Models\Merchant::$fields);
    $inp = Input::all();
    $validation = Validator::make($input, \App\Models\Merchant::$rules);

    if ($validation->passes())
    {
      $merchant = $this->merchant->find($id);
      $merchant->update($input);
      $input = Input::only('businesses', 'users', 'sales_admins');

      $users = isset($input['users']) && is_array($input['users'])?$input['users']:[];
      $_businesses = isset($inp['businesses']) && is_array($inp['businesses'])?$inp['businesses']:[];
      $merchant->allocateUsersNPocs($users, $input['sales_admins']);
      $businesses = null;
      if(count($_businesses)>0){
        $businesses =  array();
        foreach($_businesses as $ii=>$vv){
          $businesses[] = $vv;
        }
      }

      $merchant->allocateBusinesses($businesses);

      return redirect()->route('admin.merchants.show', $id);
    }

    return redirect()->route('admin.merchants.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $merchant = $this->merchant->find($id);
    $merchant->deletePre();
    $merchant->delete();
    return redirect()->route('admin.merchants.index');
  }

}
