<?php
namespace App\Http\Controllers;


use GuzzleHttp\Client as HttpClient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SmsController extends Controller {


  public function index(){
    return  redirect(route('admin.sms.get'));
  }

  public function getSend()
  {
    return  view('sms.send');
  }

  public function postSend(Request $r){
    $client = new HttpClient();

    // sms to user
    $to = $r->input('phone');
    $to = ltrim($to, '+');
    $to = ltrim($to, '0');
    $url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage';
    $url.='&send_to='.$to;
    $msg = $r->input('body');
    $url.='&msg='.urlencode($msg);
    $url.='&msg_type=TEXT&v=1.1&format=text&auth_scheme=plain';
    if(substr($to, 0, 2)=='91'){
      $url.='&userid=2000154800&password=nXOjprcpf';
    }else{
      $url.='&userid=2000155758&password=WwuTDt';
    }
    $response = $client->get($url);
    //echo $url;
    //dump($response);die();
    return redirect()->back()
      ->withInput()
      ->with('notice', 'Message(s) sent');
  }

}
