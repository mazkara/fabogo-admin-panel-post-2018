<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
 
use Validator;

class Virtual_numbersController extends Controller {

  /**
   * Virtual_number Repository
   *
   * @var Virtual_number
   */
  protected $virtual_number;

  public function __construct(\App\Models\Virtual_number $virtual_number)
  {
    $this->virtual_number = $virtual_number;
    $this->layout = 'layouts.admin-crm';
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $input =  Input::all();

    $virtual_numbers = $this->virtual_number->query();

    if(Input::has('search') && ($input['search']!='')){
      $virtual_numbers->bySearch($input['search']);
      $params['search'] = $input['search'];
    }
    
    $virtual_numbers = $virtual_numbers->byLocale()->orderBy('id', 'DESC')->paginate(20);

    //$this->layout = view('layouts.admin');
    return   view('virtual_numbers.index', compact('virtual_numbers'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return   view('virtual_numbers.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $input = Input::all();
    $validation = Validator::make($input, \App\Models\Virtual_number::$rules);

    if ($validation->passes())
    {
      $virtual_number = $this->virtual_number->create($input);
      $virtual_number->setCity();
      $virtual_number->update($input);
      $virtual_number->save();

      return redirect()->route('admin.virtual_numbers.index');
    }

    return redirect()->route('admin.virtual_numbers.create')
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $virtual_number = $this->virtual_number->findOrFail($id);
    $current_business = $virtual_number->current_business()?$virtual_number->current_business():false;

    //$this->layout = view('layouts.admin');
    return view('virtual_numbers.show', compact('virtual_number', 'current_business'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $virtual_number = $this->virtual_number->find($id);

    if (is_null($virtual_number))
    {
      return redirect()->route('virtual_numbers.index');
    }

    //$this->layout = view('layouts.admin');
    return   view('virtual_numbers.edit', compact('virtual_number'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, \App\Models\Virtual_number::$rules);

    if ($validation->passes())
    {
      $virtual_number = $this->virtual_number->find($id);
      $virtual_number->update($input);

      return redirect()->route('admin.virtual_numbers.index');
    }

    return redirect()->route('admin.virtual_numbers.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $v = $this->virtual_number->find($id);
    $v->deactivate();
    $v->save();

    return redirect()->route('admin.virtual_numbers.index');
  }

}
