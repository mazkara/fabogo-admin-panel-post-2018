<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;


class HighlightsController extends Controller {

	/**
	 * Highlight Repository
	 *
	 * @var Highlight
	 */
	protected $highlight;

	public function __construct(\App\Models\Highlight $highlight)
	{
		$this->highlight = $highlight;
	}


	public function index()
	{
		$highlights = $this->highlight->all();

		//$this->layout = view('layouts.admin');
    return   view('highlights.index', compact('highlights'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = view('layouts.admin');
    return   view('highlights.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, \App\Models\Highlight::$rules);

		if ($validation->passes())
		{
			$highlight = $this->highlight->create($input);
			
			return redirect()->route('admin.highlights.index');
		}

		return redirect()->route('admin.highlights.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$highlight = $this->highlight->findOrFail($id);

		//$this->layout = view('layouts.admin');
    return   view('highlights.show', compact('highlight'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$highlight = $this->highlight->find($id);

		if (is_null($highlight))
		{
			return redirect()->route('admin.highlights.index');
		}

		//$this->layout = view('layouts.admin');
    return   view('highlights.edit', compact('highlight'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, \App\Models\Highlight::$rules);

		if ($validation->passes())
		{
			$highlight = $this->highlight->find($id);
			$highlight->update($input);

			return redirect()->route('admin.highlights.index');
		}

		return redirect()->route('admin.highlights.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->highlight->find($id)->delete();

		return redirect()->route('admin.highlights.index');
	}

}
