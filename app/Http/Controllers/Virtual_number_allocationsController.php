<?php
namespace App\Http\Controllers;

use Validator;
//use Vinelab\Http\Client as HttpClient;

use GuzzleHttp\Client as HttpClient;

use Illuminate\Support\Facades\Input;


use App\Http\Controllers\Controller;

class Virtual_number_allocationsController extends Controller {

  /**
   * Virtual_number_allocation Repository
   *
   * @var Virtual_number_allocation
   */
  protected $virtual_number_allocation, $business, $virtual_number;

  public function __construct(\App\Models\Business $business, 
                              \App\Models\Virtual_number $virtual_number, 
                              \App\Models\Virtual_number_allocation $virtual_number_allocation)
  {
    $this->business = $business;
    $this->virtual_number = $virtual_number;

    $this->virtual_number_allocation = $virtual_number_allocation;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $virtual_number_allocations = $this->virtual_number_allocation->all();
    $businesses = $this->business->query()->byLocale();
    $params = [];
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $businesses->search($input['search']);
      $params['name'] = $input['search'];
      $params['search'] = $input['search'];
    }

    if(Input::has('sort') && ($input['sort']!='')){
      $params['sort'] = $input['sort'];
      switch($params['sort']){
        case 'idAsc':
          $businesses->orderBy('id', 'ASC');
        break; 
        case 'idDesc':
          $businesses->orderBy('id', 'DESC');
        break; 
        case 'nameAsc':
          $businesses->orderBy('name', 'ASC');
        break; 
        case 'nameDesc':
          $businesses->orderBy('name', 'DESC');
        break; 
        case 'lastUpdateAsc':
          $businesses->orderBy('updated_at', 'ASC');
        break; 
        case 'lastUpdateDesc':
          $businesses->orderBy('updated_at', 'DESC');
        break; 
      }
    }else{
      $businesses->orderBy('businesses.id', 'DESC');
    }


    $businesses = $businesses->paginate(20);

    //$this->layout = view('layouts.admin');
    return   view('virtual_number_allocations.index', compact('params', 'businesses', 'virtual_number_allocations'));
  }



  public function getAllocateForBusiness($id){
    $business = $this->business->findOrFail($id);
    $virtual_numbers = $this->virtual_number->onlyAllocatable();
    //$this->layout = view('layouts.admin');
    return   view('virtual_number_allocations.allocate', compact('virtual_numbers', 'business'));

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */

  public function create(){
    //$this->layout = view('layouts.admin');
    return   view('virtual_number_allocations.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */

  public function store()
  {
    $input = Input::except('_token', 'valid_until');
    $validation = Validator::make($input, \App\Models\Virtual_number_allocation::$rules);
//SELECT `api_virtualnumberallocation`.`id`,
//    `api_virtualnumberallocation`.`description`,
//    `api_virtualnumberallocation`.`created_at`,
//    `api_virtualnumberallocation`.`updated_at`,
//    `api_virtualnumberallocation`.`desc`,
//    `api_virtualnumberallocation`.`starts`,
//    `api_virtualnumberallocation`.`ends`,
//    `api_virtualnumberallocation`.`state`,
//    `api_virtualnumberallocation`.`phone_1`,
//    `api_virtualnumberallocation`.`phone_2`,
//    `api_virtualnumberallocation`.`phone_3`,
//    `api_virtualnumberallocation`.`is_ppl`,
//    `api_virtualnumberallocation`.`is_ivr_enabled`,
//    `api_virtualnumberallocation`.`business_id`,
//    `api_virtualnumberallocation`.`virtual_number_id`,
//    `api_virtualnumberallocation`.`old_id`
//FROM `fabogo_local`.`api_virtualnumberallocation`;

    if ($validation->passes()){
      $input['starts'] = \Carbon\Carbon::now();
      $virtual_number = \App\Models\Virtual_number::find($input['virtual_number_id']);
      //dump($input);die();
      $allocation = new \App\Models\Virtual_number_allocation();//$this->virtual_number_allocation->new();
      foreach($input as $ii=>$vv){
        $allocation->$ii = $vv;
      }

      $allocation->desc = 'Virtual number ID '.$input['virtual_number_id'].' allocated to '.$input['phone_1'];

      $allocation->save();


      //create($input);
      $client = new HttpClient();
      $url = 'http://etsintl.kapps.in/webapi/mazkara/api/mazkara_agent_mapping.py?auth_key=51c41934-94b1-4be7-82c2-6531251a76ab';
      $url .='&knowlarity_number='.$virtual_number->body.'&mapped_number='.
                                join(',', array_filter([ trim($input['phone_1']),
                                                          trim($input['phone_2']),
                                                          trim($input['phone_3'])])); 

      $response = $client->get($url);
      //$response = mzk_xml2array($response->xml());

      $business = $this->business->findOrFail($input['business_id']);

      return redirect()->route('admin.merchants.show', $business->merchant_id)
                          ->with('message', 'Success');//$response);

      return redirect()->route('admin.businesses.virtual_numbers.show', $input['business_id'])
                          ->with('message', 'Success');//$response);
    }

    return redirect()->back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $virtual_number_allocation = $this->virtual_number_allocation->findOrFail($id);

    //$this->layout = view('layouts.admin');
    return   view('administration.virtual_number_allocations.show', compact('virtual_number_allocation'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $virtual_number_allocation = $this->virtual_number_allocation->find($id);

    if (is_null($virtual_number_allocation))
    {
      return redirect()->route('virtual_number_allocations.index');
    }

    //$this->layout = view('layouts.admin');
    return   view('administration.virtual_number_allocations.edit', compact('virtual_number_allocation'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function deallocate($id)
  {
    $input = array_except(Input::all(), '_method');
    $virtual_number_allocation = $this->virtual_number_allocation->find($id);
    $virtual_number_allocation->deallocate();

    $virtual_number = \App\Models\Virtual_number::find($virtual_number_allocation->virtual_number_id);

    $client = new HttpClient();
    $url = 'http://etsintl.kapps.in/webapi/mazkara/api/mazkara_agent_mapping.py?auth_key=51c41934-94b1-4be7-82c2-6531251a76ab';
    $url .='&knowlarity_number='.$virtual_number->body.
                      '&mapped_number=None'; 

    $response = $client->get($url);
    
    //$response = mzk_xml2array($response->xml());

    return redirect()->back()->with('message', 'Ok');
  }




  public function updateVirtualNumberAllocation(){
    $input = Input::all();

    $data = ['id' => $input['pk']];

    $virtual_number_allocation = \App\Models\Virtual_number_allocation::find($data['id']);

    $virtual_number_allocation->$input['name'] = $input['value'];
    $virtual_number_allocation->save();
    return redirect()->back();

  }



  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, \App\Models\Virtual_number_allocation::$rules);

    if ($validation->passes())
    {
      $virtual_number_allocation = $this->virtual_number_allocation->find($id);
      $virtual_number_allocation->update($input);

      return redirect()->route('admin.virtual_number_allocations.show', $id);
    }

    return redirect()->route('admin.virtual_number_allocations.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $this->virtual_number_allocation->find($id)->delete();

    return redirect()->route('admin.virtual_number_allocations.index');
  }

}
