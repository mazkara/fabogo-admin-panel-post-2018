<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;


class ServicesController extends Controller {

	/**
	 * Service Repository
	 *
	 * @var Service
	 */
	protected $service;

	public function __construct(\App\Models\Service $service)
	{
		$this->service = $service;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$services = \App\Models\Service::select()->whereNull('parent_id')->get();
    return   view('services.index', compact('services'));
	}

	public function create()
	{
		$categories = \App\Models\Category::all();
    return   view('services.create', compact('categories'));
	}

	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, \App\Models\Service::$rules);
		$data = Input::only(\App\Models\Service::$fields);

		if ($validation->passes())
		{
			
			$service = $this->service->create($data);
			$cat = Input::only('categories');
			if(count($cat['categories'])>0){
				$service->categories()->attach($cat['categories']?$cat['categories']:[]);
				$service->categories()->sync($cat['categories']?$cat['categories']:[]);
			}
			
			return redirect()->route('admin.services.index');
		}

		return redirect()->route('admin.services.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function show($id){
		$service = $this->service->findOrFail($id);
    return view('services.show', compact('service'));
	}

	public function edit($id){
		$service = $this->service->find($id);

		if (is_null($service))
		{
			return redirect()->route('admin.services.index');
		}

		$categories = \App\Models\Category::all();

		//$this->layout = view('layouts.admin');
    return   view('services.edit', compact('service', 'categories'));
	}

	public function update($id)
	{
		$input = array_except(Input::all(), '_method', 'deletablePhotos');
		$validation = Validator::make($input, \App\Models\Service::$rules);
		$data = Input::only(\App\Models\Service::$fields);
		if ($validation->passes()){
			$service = $this->service->find($id);
			
			$service->update($data);
			$cat = Input::only('categories');
			if(count($cat['categories'])>0){
				$service->categories()->attach($cat['categories']?$cat['categories']:[]);
				$service->categories()->sync($cat['categories']?$cat['categories']:[]);
			}
			
			return redirect()->route('admin.services.index');
		}

		return redirect()->route('admin.services.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this->service->find($id)->delete();

		return redirect()->route('admin.services.index');
	}

}
