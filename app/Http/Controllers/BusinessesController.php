<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Storage, Response;
use GuzzleHttp\Client as HttpClient;

class BusinessesController extends Controller {

  /**
   * Brandadvert Repository
   *
   * @var Brandadvert
   */
  protected $business;

  public function __construct(\App\Models\Business $business)
  {
    $this->business = $business;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $businesses = $this->business->query()->byLocale();
    $params = Input::all();
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $businesses->searchBasic($input['search']);
      $params['name'] = $input['search'];
      $params['search'] = $input['search'];
    }
    if(Input::has('id') && ($input['id']!="")){
      $businesses->where('id', 'like', $input['id'].'%');
      $params['id'] = $input['id'];
    }

    if(Input::has('is_featured') && ($input['is_featured']!='')){
      if($input['is_featured']=='0'){
        $businesses->where('is_featured', '=', 0);
      }else{
        $businesses->where('is_featured', '>', 0);
      }
      $params['is_featured'] = $input['is_featured'];
    }

    if(Input::has('is_chain') && ($input['is_chain']!='')){
      $businesses->where('chain_id', '>', 0);
      $params['is_chain'] = $input['is_chain'];
    }

    if(Input::has('zone') && ($input['zone']!='')){
      $businesses->ofZones([$input['zone']]);
      $params['zone'] = $input['zone'];
    }

    if(Input::has('chain') && ($input['chain']!='')){
      $businesses->ofChains([$input['chain']]);
      $params['chain'] = $input['chain'];
    }

    if(Input::has('sort') && ($input['sort']!='')){
      $params['sort'] = $input['sort'];
      switch($params['sort']){
        case 'idAsc':
          $businesses->orderBy('id', 'ASC');
        break; 
        case 'idDesc':
          $businesses->orderBy('id', 'DESC');
        break; 
        case 'nameAsc':
          $businesses->orderBy('name', 'ASC');
        break; 
        case 'nameDesc':
          $businesses->orderBy('name', 'DESC');
        break; 
        case 'lastUpdateAsc':
          $businesses->orderBy('updated_at', 'ASC');
        break; 
        case 'lastUpdateDesc':
          $businesses->orderBy('updated_at', 'DESC');
        break; 
      }
    }else{
      $businesses->orderBy('id', 'DESC');
    }

    $businesses = $businesses->paginate(20);
    return view('businesses.index', compact('businesses', 'params'));
  }


  public function updatable(){
    $input = Input::all();

    $data = ['id' => $input['pk']];

    $advert = $this->advert->find($data['id']);

    $campaign = \App\Models\Campaign::find($advert->campaign_id);

    if($input['name'] == 'start_date'){
      if(strtotime($input['value']) < strtotime($campaign->start_date)){
        //error
        return response('Error! Start date cannot be earlier than campaign start date', 400);

      }elseif(strtotime($input['value']) > strtotime($advert->end_date)){
        return response('Error! Start date cannot be later than ad end date', 400);
      }else{
        $advert->start_date = $input['value'];
        $advert->save();

      }
    }elseif($input['name'] == 'end_date'){
      if(strtotime($input['value']) > strtotime($campaign->end_date)){
        //error
        return response('Error! End date cannot be later than campaign end date', 400);
      }elseif(strtotime($input['value']) < strtotime($advert->start_date)){
        return response('Error! End date cannot be earlier than ad start date', 400);
      }else{
        $advert->end_date = $input['value'];
        $advert->save();
      }
    }

    return response('Success', 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $services = \App\Models\Service::select()->where('parent_id', '=', 0)->orWhereNull('parent_id')->get()->toArray();

    foreach($services as $ii=>$service){
      $services[$ii]['children'] = \App\Models\Service::select()->where('parent_id', '=', $service['id'])->orderby('name', 'asc')->get()->toArray();
    }


    $zones = \App\Models\Zone::select()->byLocale()->where('depth', '=', 1)->get()->toArray();

    foreach($zones as $ii=>$zone){
      $zones[$ii]['children'] = \App\Models\Zone::select()->byLocale()->where('parent_id', '=', $zone['id'])->orderby('name', 'asc')->get()->toArray();
    }

    $categories = \App\Models\Category::select()->orderby('name', 'asc')->get();
    $highlights = \App\Models\Highlight::select()->orderby('name', 'asc')->get();

    $selected_categories = [];
    $selected_services = [];
    $selected_highlights = [];

    return view('businesses.create', compact('services','zones','categories','highlights','selected_categories','selected_services','selected_highlights'));
  }



  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $input = Input::all();

    $validation = Validator::make($input, \App\Models\Business::$rules);

    if ($validation->passes()){
      $business_data = Input::only(\App\Models\Business::$fields);
      $business = new \App\Models\Business();

      foreach(\App\Models\Business::$fields as $field){
        if(isset($business_data[$field])){
          $business->$field = $business_data[$field];
        }
      }

      $business->save();

      $business_details = new \App\Models\Business_details();//::create($business_details_data);
      $business_details_data = Input::only(\App\Models\Business_details::$fields);

      foreach(\App\Models\Business_details::$fields as $field){
        if(isset($business_details_data[$field])){
          $business_details->$field = $business_details_data[$field];
        }
      }

      $business_details->business_id = $business->id;
      $business_details->save();

      $highlights = Input::get('highlights');
      $categories = Input::get('categories');

      $business->highlights()->sync($highlights);
      $business->categories()->sync($categories);

      $zone = \App\Models\Zone::find($business->zone_id);
      $city = \App\Models\City::find($zone->city_id);
      $str = http_build_query(['name'=>$business->name, 'zone_id'=>$zone->old_id, 'city_id'=>$city->old_id]);
      $url = config('app.old_site_url').'/api/businesses/create?'.$str;
      $client = new HttpClient();
      $response = $client->get($url);
      $response = json_decode($response->getBody());

      $business->old_id = $response->id;
      $business->save();

      return redirect()->route('admin.businesses.show', $business);
    }

    return redirect()->back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }


  public function generateOldId($id){
    $business = $this->business->findOrFail($id);
    if($business->old_id == 0){
      $zone = \App\Models\Zone::find($business->zone_id);
      $city = \App\Models\City::find($zone->city_id);
      $str = http_build_query(['name'=>$business->name, 'zone_id'=>$zone->old_id, 'city_id'=>$city->old_id]);
      $url = config('app.old_site_url').'/api/businesses/create?'.$str;
      $client = new HttpClient();
      $response = $client->get($url);
      $response = json_decode($response->getBody());

      $business->old_id = $response->id;
      $business->save();
    }

    return redirect()->route('admin.businesses.show', $business);
  }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id){
    $business = $this->business->findOrFail($id);
    return view('businesses.show', compact('business'));
  }


  public function getTimings($id)
  {
    $business = $this->business->findOrFail($id);

    //$this->layout = View::make('layouts.admin');
    return view('businesses.timings', compact('business'));
  }


  public function postTimings($id)
  {
    $business = $this->business->findOrFail($id);
    $timings = Input::only('timings');
    $business->saveTimings($timings['timings']);

    return redirect()->route('admin.businesses.show', $id);
  }

  public function deleteOneBusinessService($id){
    $service = \App\Models\Business_service::find($id);
    $service->delete();

    return redirect()->back();
  }


  public function toggleFeatured($id){
    $business = $this->business->findOrFail($id);
    if($business->isFeatured()){
      $business->is_featured = 0;
    }else{
      $business->is_featured = 1;
    }

    $business->save();
    return redirect()->back();
  }

  public function getCopyServices($id){
    $business = $this->business->findOrFail($id);
    $venues = $business->group->businesses;
    return view('businesses.services-copy', compact('business', 'venues'));
  }

  public function postCopyServices($id){
    $business = $this->business->findOrFail($id);
    $input = Input::all();
    
    $service_ids = [];
    $services = \App\Models\Business_service::select()->where('business_id', '=', $id)->get();
    foreach($input['ids'] as $b_id):
      foreach($services as $service){
        $service_ids[] = $service->service_id;
        $business_service = \App\Models\Business_service::firstOrNew(array('service_id'=>$service->service_id, 'business_id'=>$b_id));

        foreach(\App\Models\Business_service::$fields as $f){
          if(!in_array($f, ['service_id', 'business_id'])){
            $business_service->$f = $service->$f;
          }
        }
        
        $business_service->service_id = $service->service_id;
        $business_service->business_id = $b_id;
        $business_service->save();
      }
    endforeach;
    foreach($input['ids'] as $b_id):
      $deletables = \App\Models\Business_service::select()->where('business_id', '=', $b_id)->whereNotIn('service_id', $service_ids)->pluck('id', 'id')->all();
      if(count($deletables)>0){
        \DB::table('api_businessservice')->whereIn('id', $deletables)->delete();
      }
    endforeach;
    return redirect()->route('admin.businesses.show', $id);
  }


  public function getServices($id){
    $business = $this->business->findOrFail($id);
    $services = \App\Models\Service::select()->whereNull('parent_id')->get();
    $business_services = $business->services()->get()->toArray();
    $selected_services = array();


    foreach($business_services as $one_service){
      $selected_services[$one_service['pivot']['service_id']] = $one_service['pivot'];
    }


    return view('businesses.services', compact('business', 'services', 'selected_services'));
  }

  public function postServices($id){
    $business = $this->business->findOrFail($id);

    $input = Input::all();
    $data = array();
    $service_ids = [];

    foreach($input['services'] as $service_id=>$one_service){
      $service_ids[] = $service_id;
      $row = $one_service;

      unset($row['available']);
      $row['service_id'] = $service_id;
      $row['discount_percentage'] = (int)$row['discount_percentage'];
      $row['description'] = empty(trim($row['description'])) ? $row['name'] : $row['description'];

      if($row['discount_percentage']>0){
        $row['discounted_price'] = ($row['discount_percentage']*$row['price'])/100;
        $row['final_price'] = $row['price'] - $row['discounted_price'];
        $row['lowest_price'] = $row['final_price'];
      }else{
        $row['discounted_price'] = $row['price'];
        $row['final_price'] = $row['price'];
        $row['lowest_price'] = $row['price'];
      }

      $business_service = \App\Models\Business_service::firstOrNew(array('service_id'=>$service_id, 'business_id'=>$id));

      foreach(\App\Models\Business_service::$fields as $f){
        if(isset($row[$f])){
          $business_service->$f = $row[$f];
        }
      }

      $business_service->business_id = $id;
      $business_service->save();
      $data[$service_id] = $row;
    }

    $deletables = \App\Models\Business_service::select()->where('business_id', '=', $id)->whereNotIn('service_id', $service_ids)->pluck('id', 'id')->all();
    if(count($deletables)>0){
      \DB::table('api_businessservice')->whereIn('id', $deletables)->delete();
    }

    return redirect()->route('admin.businesses.show', $id);
  }

  public function getLocation($id){
    $business = $this->business->findOrFail($id);

    //$this->layout = View::make('layouts.admin');
    return view('businesses.location', compact('business'));
  }


  public function postLocation($id)
  {
    $business = $this->business->findOrFail($id);
    $data = Input::only(\App\Models\Business::$fields);
      foreach(\App\Models\Business::$fields as $field){
        if(isset($data[$field])){
          $business->$field = $data[$field];
        }
      }

    $business->save();
    return redirect()->route('admin.businesses.show', $id);
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $business = $this->business->findOrFail($id);

    $services = \App\Models\Service::select()->where('parent_id', '=', 0)->orWhereNull('parent_id')->get()->toArray();

    foreach($services as $ii=>$service){
      $services[$ii]['children'] = \App\Models\Service::select()->where('parent_id', '=', $service['id'])->orderby('name', 'asc')->get()->toArray();
    }


    $zones = \App\Models\Zone::select()->byLocale()->where('depth', '=', 1)->get()->toArray();

    foreach($zones as $ii=>$zone){
      $zones[$ii]['children'] = \App\Models\Zone::select()->byLocale()->where('parent_id', '=', $zone['id'])->orderby('name', 'asc')->get()->toArray();
    }

    $categories = \App\Models\Category::select()->orderby('name', 'asc')->get();
    $highlights = \App\Models\Highlight::select()->orderby('name', 'asc')->get();

    $selected_categories = $business->categories()->pluck('category_id', 'category_id')->all();
    $selected_services = [];
    $selected_highlights = $business->highlights()->pluck('highlights_id', 'highlights_id')->all();

    return view('businesses.edit', compact('business','services','zones','categories','highlights','selected_categories','selected_services','selected_highlights'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');

    $business = $this->business->find($id);

    $validation = Validator::make($input, \App\Models\Business::$rules);

    if ($validation->passes()){
      $business_data = Input::all();

      foreach(\App\Models\Business::$fields as $field){
        if(array_key_exists($field, $business_data)){
          $business->$field = $business_data[$field];
        }
      }

      $business->save();

      $business_details = \App\Models\Business_details::firstOrNew(['business_id'=>$id]);//::create($business_details_data);
      $business_details_data = Input::only(\App\Models\Business_details::$fields);

      foreach(\App\Models\Business_details::$fields as $field){
        if(array_key_exists($field, $business_details_data)){
          $business_details->$field = $business_details_data[$field];
        }
      }

      if($input['is_featured']>0){
        $business->updateFeatured();
        $business->save();
      }

      $business_details->business_id = $business->id;
      $business_details->save();

      $highlights = Input::get('highlights');
      $categories = Input::get('categories');

      $business->highlights()->sync($highlights);
      $business->categories()->sync($categories);

      return redirect()->route('admin.businesses.show', $business);
    }

    return redirect()->back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  public function resluggify($id){
    $business = $this->business->findOrFail($id);
    $business->slug = null;
    $business->save();

    return redirect()->back();
  }

  public function getModules($id){
    $business = $this->business->findOrFail($id);
    $modules = \App\Models\Module::select()->get();
    $result = ['title'=>'Modules Access for '.$business->name, 'body'=>''];

    $selected_modules = array();
    $selected_modules = \App\Models\Module_access::where('business_id', '=', $id)->pluck('access', 'module_id')->all();
    $result['body'] = view('businesses.modules', compact('business', 'modules', 'selected_modules'))->render();
    return Response::json($result);
  }

  public function postModules($id){
    $business = $this->business->findOrFail($id);
    $data = Input::all();

    foreach($data['modules'] as $module_id=>$access){
      $module_access = \App\Models\Module_access::firstOrNew(array('module_id'=>$module_id, 'business_id'=>$id));
      $module_access->module_id = $module_id;
      $module_access->business_id = $id;

      $module_access->access = is_null($access['active'])?0:$access['active'];
      
      $module_access->save();
    }

    return redirect()->back();
  }

  public function getPhotos($id)
  {
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }

    //$this->layout = View::make('layouts.admin');
    return view('businesses.photos', compact('business'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */

  public function postPhotos(Request $request, $id)
  {
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }

    $images = $request->file('images');

    if($images){
      $business->uploadPhotos($images, 'venue-image');
    }

    $orderables = Input::only('orderables');

    $is_cover = Input::only('is_cover');
    if(isset($is_cover['is_cover']) && ($is_cover['is_cover']>0)){
      $business->photos()->update(['image_type'=>'venue-image']);

      $p = \App\Models\Image::find($is_cover['is_cover']);

      $p->image_type = 'cover-image';
      $p->save();

    }

    if(isset($orderables['orderables'])):
      $orderables = Input::only('orderables');
      $orderables = is_array($orderables)?$orderables:[];

      foreach($orderables['orderables'] as $p_id=>$p_order){
        $p = \App\Models\Image::find($p_id);
        $p->slot = $p_order;
        $p->save();
      }
    endif;

    $deletablePhotos = Input::only('deletablePhotos');
    $deletablePhotos = isset($deletablePhotos['deletablePhotos']) && is_array($deletablePhotos['deletablePhotos']) ? $deletablePhotos['deletablePhotos']:[];
    foreach($deletablePhotos as $p_id){
      $p = \App\Models\Image::find($p_id);
      $p->delete();
    }
    return redirect()->back();
  }

  public function postStockPhoto(Request $request, $id){
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }

    $cover_image_url = $request->input('cover_image');

    if(empty($cover_image_url)){
      return redirect()->back();
    }

    $business->setCoverImage($cover_image_url);

    return redirect()->back();

  }



  public function getOffers($id){
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }
    return view('businesses.offers', compact('business'));
  }

  public function postOffers($id){
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }

    $offers = Input::only('offers');
    $offers = (isset($offers['offers']) && is_array($offers['offers'])) ? $offers['offers']:[];
    $business->details->offers = $offers;
    $business->details->save();
    return redirect()->route('admin.businesses.show', $business);
  }



  public function getRateCards($id)
  {
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }

    return view('businesses.rate-cards', compact('business'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */

  public function postRateCards(Request $request, $id)
  {
    $business = $this->business->findOrFail($id);
    if(!isset($business->id)){
      return $business; 
    }

    $images = $request->file('images');

    if($images){
      $business->uploadPhotos($images, 'rate-card');
    }

    $orderables = Input::only('orderables');

    if(isset($orderables['orderables'])):
      $orderables = Input::only('orderables');
      $orderables = is_array($orderables)?$orderables:[];

      foreach($orderables['orderables'] as $p_id=>$p_order){
        $p = \App\Models\Image::find($p_id);
        $p->slot = $p_order;
        $p->save();
      }
    endif;

    $deletablePhotos = Input::only('deletablePhotos');
    $deletablePhotos = isset($deletablePhotos['deletablePhotos']) && is_array($deletablePhotos['deletablePhotos']) ? $deletablePhotos['deletablePhotos']:[];
    foreach($deletablePhotos as $p_id){
      $p = \App\Models\Image::find($p_id);
      $p->delete();
    }
    return redirect()->back();
  }

}
