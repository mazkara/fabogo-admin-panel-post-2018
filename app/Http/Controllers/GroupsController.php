<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Storage;

class GroupsController extends Controller {

  /**
   * Group Repository
   *
   * @var Group
   */
  protected $group;

  public function __construct(\App\Models\Group $group)
  {
    $this->group = $group;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $groups = $this->group->query();

    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $groups->where('name', 'like', $input['search'].'%');
    }

    $groups = $groups->byLocale()->byType('chain')->orderby('name', 'asc')->paginate(20);//->get();

    //$this->layout = view('layouts.admin');
    return  view('groups.index', compact('groups'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //$this->layout = view('layouts.admin');
    return  view('groups.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $input = Input::all();
    $validation = Validator::make($input, \App\Models\Group::$rules);

    if ($validation->passes()){

      $group = new \App\Models\Group();

      foreach(\App\Models\Group::$fields as $field){
        if(isset($input[$field])){
          $group->$field = $input[$field];
        }
      }

      $group->city_id = mzk_get_localeID();
      $group->type = 'chain';

      $group->save();


      if(isset($input['cover'])){
        $group->savePhoto($input['cover']);
      }

      return redirect()->route('admin.groups.index');
    }

    return redirect()->route('admin.groups.create')
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $group = $this->group->findOrFail($id);

    return  view('groups.show', compact('group'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $group = $this->group->find($id);

    if (is_null($group))
    {
      return redirect()->route('admin.groups.index');
    }

    //$this->layout = view('layouts.admin');
    return  view('groups.edit', compact('group'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, \App\Models\Group::$rules);

    if ($validation->passes())
    {
      $group = $this->group->find($id);
      if(isset($input['deletablePhotos'])){
        $group->removePhoto();
      }

      foreach(\App\Models\Group::$fields as $field){
        if(isset($input[$field])){
          $group->$field = $input[$field];
        }
      }
      $group->type = 'chain';
      
      $group->save();
      if(isset($input['cover'])){
        $group->savePhoto($input['cover']);
      }

      return redirect()->route('admin.groups.show', $id);
    }

    return redirect()->route('admin.groups.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $this->group->find($id)->delete();

    return redirect()->route('admin.groups.index');
  }

}
