<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator, Storage, Response;

class SiteController extends Controller{

  public function login(Request $request){
    $id = $request->input('id');
    $token = $request->input('token');
    
    $user = \App\Models\User::find($id);

    if(!$user){
      return redirect()->to('/');
    }

    if($user->access_token!=$token){
      //return redirect()->to('/');
    }

    $allowed_roles = array('admin', 'sales', 'moderator', 'finance', 'sales-admin');
    $roles = explode(',', $user->role);
    $roles_existing = array_intersect($roles, $allowed_roles);

    if(count($roles_existing)==0){
      return redirect()->to('/');
    }

    \Auth::loginUsingId($id);
    return redirect('/');//view('site.home');
  }

  public function logout(){
    \Auth::logout();
    return view('site.logout');
  }

  public function index(Request $request){

    if(!\Auth::check()){
      return view('site.404');
    }

    $dashboards = ['crm', 'content'];
    return view('site.home', compact('dashboards'));
  }

  public function contentDashboard(){
    return redirect('/content/businesses');
  }

  public function crmDashboard(){
    return redirect('/crm/campaigns');
  }

  public function changeLocale($id){
    $city = \App\Models\City::find($id);
    mzk_set_default_city($city);
    return redirect()->back();
  }

  public function uploadFile(Request $request){
    $result = array();

    $file_names = array();
    foreach($_FILES as $file_name=>$one_file){
      $photo = $request->file($file_name);

      $name = $photo->getClientOriginalName();
      $image_path = $photo->getPathName();
      $image_large = \Image::make($image_path)->resize(800, null, function ($constraint) {
        $constraint->aspectRatio();
      });

      $image_thumbnail = \Image::make($image_path)->fit(400, 275);

      $image_large = $image_large->stream();
      $image_thumbnail = $image_thumbnail->stream();

      $folder = '/images/'.str_slug($file_name).'_'.time();

      $large_path = $folder.'/large/'.$name;
      $thumb_path = $folder.'/medium/'.$name;

      \Storage::disk('s3')->put($large_path, $image_large->__toString(), 'public');
      \Storage::disk('s3')->put($thumb_path, $image_thumbnail->__toString(), 'public');
      $img = new \App\Models\Image();
      $img->description = $name;
      $img->name = $name;
      $img->image_url = 'https://s3.amazonaws.com/mazkaracdn'.$large_path;
      $img->image_thumbnail_url = 'https://s3.amazonaws.com/mazkaracdn'.$thumb_path;
      $img->save();
      $result[] = array('id'=>$img->id, 
                        'url'=>$img->image_url, 
                        'thumb'=>$img->image_thumbnail_url);
    }

    return Response::json($result);
  }


}

