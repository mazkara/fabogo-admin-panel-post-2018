<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use \App\Models\Merchant;

class MerchantCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $merchant;

    public function __construct(\App\Models\Merchant $merchant)
    {
        $this->merchant = $merchant;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
