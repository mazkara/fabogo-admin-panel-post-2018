<?php

namespace App\Listeners\Merchant;

use App\Events\MerchantCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetAttributables
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BusinessUpdated  $event
     * @return void
     */
    public function handle(MerchantCreated $event)
    {
        $merchant = $event->merchant;
        $merchant->generateGlobalID();
    }
}
