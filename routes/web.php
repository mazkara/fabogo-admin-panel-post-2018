<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::get('/auth/login', 'SiteController@login');

Route::group(['middleware'=>['restrict.access']], function(){

  Route::group(array('prefix'=>'sms' ), function(){
    Route::get('/', ['as'=>'admin.sms.index', 'uses'=>'SmsController@index']);
    Route::get('send', ['as'=>'admin.sms.get', 'uses'=>'SmsController@getSend']);
    Route::post('send', ['as'=>'admin.sms.post', 'uses'=>'SmsController@postSend']);
  });


Route::group(array('prefix'=>'content'), function(){
  Route::get('/', ['as'=>'content.dashboard', 'uses'=>'SiteController@contentDashboard']);
  Route::resource('businesses', 'BusinessesController', array('names' => mzk_route_helper('businesses', 'admin')));
  Route::resource('groups', 'GroupsController', array('names' => mzk_route_helper('groups', 'admin')));
  Route::resource('categories', 'CategoriesController', array('names' => mzk_route_helper('categories', 'admin')));
  Route::resource('zones', 'ZonesController', array('names' => mzk_route_helper('zones', 'admin')));
  Route::resource('highlights', 'HighlightsController', array('names' => mzk_route_helper('highlights', 'admin')));
  Route::resource('cities', 'CitiesController', array('names' => mzk_route_helper('cities', 'admin')));
  Route::resource('services', 'ServicesController', array('names' => mzk_route_helper('services', 'admin')));

  Route::get('businesses/{id}/photos', ['as'=>'admin.businesses.show.photos', 'uses'=>'BusinessesController@getPhotos']);
  Route::post('businesses/{id}/photos', ['as'=>'admin.businesses.save.photos', 'uses'=>'BusinessesController@postPhotos']);
  Route::post('businesses/{id}/bulk/photos', ['as'=>'admin.businesses.save.bulk.photos', 'uses'=>'BusinessesController@postBulkPhotos']);

  Route::post('businesses/{id}/stock/photos', ['as'=>'admin.businesses.save.stock.photo', 'uses'=>'BusinessesController@postStockPhoto']);

  Route::get('businesses/{id}/rate-cards', ['as'=>'admin.businesses.show.rate_cards', 'uses'=>'BusinessesController@getRateCards']);
  Route::post('businesses/{id}/rate-cards', ['as'=>'admin.businesses.save.rate_cards', 'uses'=>'BusinessesController@postRateCards']);

  Route::get('businesses/{id}/services', ['as'=>'admin.businesses.show.services', 'uses'=>'BusinessesController@getServices']);
  Route::post('businesses/{id}/services', ['as'=>'admin.businesses.save.services', 'uses'=>'BusinessesController@postServices']);

  Route::get('businesses/{id}/copy-services', ['as'=>'admin.businesses.copyable.services', 'uses'=>'BusinessesController@getCopyServices']);
  Route::post('businesses/{id}/copy-services', ['as'=>'admin.businesses.copy.services', 'uses'=>'BusinessesController@postCopyServices']);

  Route::get('businesses/{id}/modules', ['as'=>'admin.businesses.show.modules', 'uses'=>'BusinessesController@getModules']);
  Route::post('businesses/{id}/modules', ['as'=>'admin.businesses.save.modules', 'uses'=>'BusinessesController@postModules']);

  Route::get('businesses/{id}/toogle-featured', ['as'=>'admin.businesses.toggle-featured', 'uses'=>'BusinessesController@toggleFeatured']);
  Route::get('businesses/{id}/resluggify', ['as'=>'admin.businesses.resluggify', 'uses'=>'BusinessesController@resluggify']);
  Route::get('businesses/{id}/offers', ['as'=>'admin.businesses.show.offers', 'uses'=>'BusinessesController@getOffers']);
  Route::post('businesses/{id}/offers', ['as'=>'admin.businesses.save.offers', 'uses'=>'BusinessesController@postOffers']);

  Route::get('businesses/{id}/timings', ['as'=>'admin.businesses.get.timings', 'uses'=>'BusinessesController@getTimings']);
  Route::post('businesses/{id}/timings', ['as'=>'admin.businesses.post.location', 'uses'=>'BusinessesController@postTimings']);
  Route::get('businesses/{id}/location', ['as'=>'admin.businesses.get.location', 'uses'=>'BusinessesController@getLocation']);
  Route::post('businesses/{id}/location', ['as'=>'admin.businesses.post.location', 'uses'=>'BusinessesController@postLocation']);
  Route::post('businesses/{id}/delete-service', ['as'=>'admin.businesses.services.destroy', 'uses'=>'BusinessesController@deleteOneBusinessService']);

  Route::get('businesses/{id}/generate-old-id', ['as'=>'admin.businesses.generate-old-id', 'uses'=>'BusinessesController@generateOldId']);

});


Route::group(array('prefix'=>'crm'), function(){
  Route::get('/', ['as'=>'crm.dashboard', 'uses'=>'SiteController@crmDashboard']);

  Route::resource('virtual_number_allocations', 'Virtual_number_allocationsController', array('names' => mzk_route_helper('virtual_number_allocations', 'admin')));


  Route::post('upload-file', ['as'=>'crm.file-upload', 'uses'=>'SiteController@uploadFile']);

  Route::get('businesses/{id}/virtual_numbers/allocate', ['as'=>'admin.virtual_numbers.for.business.allocate',
                                'uses'=>'Virtual_number_allocationsController@getAllocateForBusiness']);
  Route::get('businesses/{id}/virtual_numbers/deallocate', ['as'=>'admin.virtual_numbers.for.business.deallocate',
                                'uses'=>'Virtual_number_allocationsController@deallocate']);
  Route::get('businesses/{id}/virtual_numbers', ['as'=>'admin.businesses.virtual_numbers.show',
                                          'uses'=>'BusinessesController@getVirtualNumberAllocation']);
  Route::get('businesses/{id}/call_logs', ['as'=>'admin.businesses.call_logs',
                                          'uses'=>'Call_logsController@getIndex']);

  Route::get('adverts/get/avalable/slots', ['as'=>'crm.adverts.get.slots', 'uses'=>'AdvertsController@getAvailableSlots']);

  Route::delete('campaigns/adverts/delete/{id}',array('uses' => 'AdvertsController@deleteOne', 'as' => 'admin.adverts.single.destroy'));
  Route::get('campaigns/edit-allocated-slots/{id}', ['as'=>'crm.campaigns.edit.slots', 'uses'=>'CampaignsController@getEditableAverts']);

  Route::resource('adverts', 'AdvertsController', array('names' => mzk_route_helper('adverts', 'admin')));
  Route::resource('campaigns', 'CampaignsController', array('names' => mzk_route_helper('campaigns', 'admin')));
  Route::resource('wikis', 'WikisController', array('names' => mzk_route_helper('wikis', 'admin')));
 
  Route::post('adverts/updatable', ['uses'=>'AdvertsController@updatable', 'as'=>'admin.adverts.updatable']);

  Route::get('merchants/call-logs/get-businesses', ['as'=>'admin.merchants.call_logs.businesses', 'uses'=>'MerchantsController@getOutletsForMerchantInvoiceForm']);

  Route::get('merchants/toggle-leads-access-for-business/{id}', ['as'=>'admin.merchants.toggle_leads_access.businesses', 'uses'=>'MerchantsController@toggleLeadsAccessForBusiness']);
  Route::get('merchants/toggle-resumes-access-for-business/{id}', ['as'=>'admin.merchants.toggle_resumes_access.businesses', 'uses'=>'MerchantsController@toggleResumesAccessForBusiness']);
  Route::get('merchants/toggle-debug-for-business/{id}', ['as'=>'admin.merchants.toggle_debug.businesses', 'uses'=>'BusinessesController@toggleDebug']);


  Route::get('merchants/export', ['as'=>'admin.merchants.export', 'uses'=>'MerchantsController@export']);
  Route::get('merchants/global', ['as'=>'admin.merchants.global', 'uses'=>'MerchantsController@globalIndex']);
  Route::get('merchants/export/global', ['as'=>'admin.merchants.export.global', 'uses'=>'MerchantsController@exportGlobalIndex']);

  Route::get('merchants/access/{merchant_id}/{user_id}', ['as'=>'admin.merchants.access.form', 'uses'=>'MerchantsController@getFormForModuleAccess']);
  Route::post('merchants/access/post', ['as'=>'admin.merchants.access', 'uses'=>'MerchantsController@postModuleAccess']);

  Route::get('merchants/get-potential-clients', ['as'=>'admin.merchants.potential.clients', 'uses'=>'MerchantsController@getUsersForMerchant']);
  Route::get('merchants/get-clients-outlets', ['as'=>'admin.merchants.client.outlets', 'uses'=>'MerchantsController@getOutletsForMerchant']);
  Route::resource('merchants', 'MerchantsController', array('names' => mzk_route_helper('merchants', 'admin')));

  Route::get('merchants/call-logs/get-businesses', ['as'=>'admin.merchants.call_logs.businesses', 'uses'=>'MerchantsController@getOutletsForMerchantInvoiceForm']);

  Route::resource('virtual_number_allocations', 'Virtual_number_allocationsController', array('names' => mzk_route_helper('virtual_number_allocations', 'admin')));

  Route::resource('virtual_numbers', 'Virtual_numbersController', array('names' => mzk_route_helper('virtual_numbers', 'admin')));

});

  Route::get('session', function() {
      return Session::all();
  });

  Route::get('/welcome', function () {
    return view('welcome');
  });
  Route::get('/', 'SiteController@index');

  Route::get('/change/locale/{id}', ['uses'=>'SiteController@changeLocale', 'as'=>'change.locale']);
  Route::get('/auth/logout', ['as'=>'users.logout', 'uses'=>'SiteController@logout']);
});
  
});
